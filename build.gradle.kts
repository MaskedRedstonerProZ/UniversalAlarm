plugins {
    id("com.android.application") version "8.2.2" apply false
    id("com.android.library") version "8.2.2" apply false
    id("app.cash.sqldelight") version "2.0.0-rc01" apply false
    kotlin("android") version "1.9.0" apply false
    kotlin("plugin.serialization") version "1.9.0" apply false
}

tasks.create<Delete>("clean") {
    delete(rootProject.buildDir)
}
