package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.repository.AlarmRepository
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions.AlarmLabelLengthOverflowException
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions.AlarmActivePeriodStartAfterEndException
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions.AlarmWeeklyRepetitionSettingsBlankException
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.Constants
import kotlin.jvm.Throws

/**
 * The use case for getting a single [Alarm]
 * @param repository The [AlarmRepository] for accessing the database
 * @throws AlarmLabelLengthOverflowException If the [Alarm] label overflows the maximum character length
 * @throws AlarmActivePeriodStartAfterEndException If the [Alarm] happens to have the endDate of an active period set to a date before the one set as the startDate
 * @throws AlarmWeeklyRepetitionSettingsBlankException if the [Alarm] happens to have the weekly repetition setting array contain an empty string
 * @author MaskedRedstonerProZ
 */
class AddAlarm(
    private val repository: AlarmRepository
) {

    @Throws(
        AlarmLabelLengthOverflowException::class,
        AlarmActivePeriodStartAfterEndException::class,
        AlarmWeeklyRepetitionSettingsBlankException::class
    )
    // invocation operator
    suspend operator fun invoke(alarm: Alarm) {

        if(alarm.label.length > Constants.MAX_ALARM_LABEL_LENGTH) throw AlarmLabelLengthOverflowException(alarm.label.length)

        alarm.activePeriodsAndWeekdays.keys.forEach { (startDate, endDate) ->
            if(startDate.isAfter(endDate)) throw AlarmActivePeriodStartAfterEndException(startDate, endDate)
        }

        if (alarm.activePeriodsAndWeekdays.isEmpty()) throw AlarmWeeklyRepetitionSettingsBlankException()

        alarm.activePeriodsAndWeekdays.values.forEach { repetitionSettings ->
            if(repetitionSettings.isEmpty()) throw AlarmWeeklyRepetitionSettingsBlankException()
        }

        alarm.activePeriodsAndWeekdays.values.forEach { repetitionSettings ->
            repetitionSettings.forEach {
                if(it.isEmpty()) throw AlarmWeeklyRepetitionSettingsBlankException()
            }
        }

        repository.insertAlarm(alarm.label, alarm.dueTime, alarm.activePeriodsAndWeekdays)
    }

}