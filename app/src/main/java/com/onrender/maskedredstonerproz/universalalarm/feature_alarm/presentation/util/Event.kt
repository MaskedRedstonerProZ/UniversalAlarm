package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util

import java.time.LocalDate
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import java.time.LocalTime

/**
 * Event manager
 * @author MaskedRedstonerProZ
 */
sealed class Event {

    /**
     * Event that is triggered when alarms are to be retrieved from the database
     * @author MaskedRedstonerProZ
     */
    data object FetchAlarmsFromDatabase: Event()

    /**
     * Event that triggers when the [Alarm] creation dialog is to be shown
     * @author MaskedRedstonerProZ
     */
    data object ShowCreateNewAlarmDialog: Event()

    /**
     * Event that triggers when the [Alarm] creation dialog is to be hidden
     * @author MaskedRedstonerProZ
     */
    data object HideCreateNewAlarmDialog: Event()

    /**
     * Event that triggers when the [Alarm] label text is to be updated
     * @param value The new [Alarm] label text
     * @author MaskedRedstonerProZ
     */
    data class UpdateAlarmLabel(val value: String): Event()

    /**
     * Event that triggers when the [Alarm] due time is to be updated
     * @param value The new due time
     * @author MaskedRedstonerProZ
     */
    data class UpdateAlarmDueTime(val value: LocalTime): Event()


    /**
     * Event that triggers when the [Alarm] active periods and weekdays map is to be updated
     * @param value The new active periods and weekdays map
     * @author MaskedRedstonerProZ
     */
    data class UpdateActivePeriodsAndWeekdaysMap(val value: Map<Pair<LocalDate, LocalDate>, Array<String>>): Event()


    /**
     * Event that triggers when an [Alarm] is to be added to the database
     * @author MaskedRedstonerProZ
     */
    data object AddAlarmToDatabase: Event()

    /**
     * Event that triggers when an [Alarm] is to be deleted
     * @param index The index of the [Alarm] to be deleted
     * @author MaskedRedstonerProZ
     */
    data class DeleteAlarmFromDatabase(val index: Int): Event()

    /**
     * Event that triggers when an [Alarm] is to be scheduled
     * @param index The index of the [Alarm] to be scheduled
     * @author MaskedRedstonerProZ
     */
    data class ScheduleAlarm(val index: Int): Event()

    /**
     * Event that triggers when an [Alarm] is to be cancelled
     * @param index The index of the [Alarm] to be cancelled
     * @author MaskedRedstonerProZ
     */
    data class CancelAlarm(val index: Int): Event()

    /**
     * Event that triggers when the alarm rescheduling work is to be started
     * @author MaskedRedstonerProZ
     */
    data object StartRescheduleWork: Event()

    /**
     * Event that triggers when the alarm rescheduling work is to be stopped
     * @author MaskedRedstonerProZ
     */
    data object StopRescheduleWork: Event()
}
