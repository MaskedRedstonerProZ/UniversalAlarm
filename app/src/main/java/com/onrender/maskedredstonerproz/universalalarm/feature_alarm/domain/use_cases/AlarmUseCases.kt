package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.*

/**
 * The collection of [Alarm] related use cases
 * @param getAlarm The use case for getting a single [Alarm]
 * @param getAllAlarms The use case for getting all the [Alarm]s
 * @param deleteAlarm The use case for deleting an [Alarm]
 * @param addAlarm The use case for adding an [Alarm]
 * @param scheduleAlarm The use case for scheduling [Alarm]s
 * @param startWork The use case for starting the work manager worker.
 * @author MaskedRedstonerProZ
 */
data class AlarmUseCases(
    val getAlarm: GetAlarm,
    val getAllAlarms: GetAllAlarms,
    val deleteAlarm: DeleteAlarm,
    val addAlarm: AddAlarm,
    val scheduleAlarm: ScheduleAlarm,
    val cancelAlarm: CancelAlarm,
    val startWork: StartWork,
    val cancelWork: CancelWork
)
