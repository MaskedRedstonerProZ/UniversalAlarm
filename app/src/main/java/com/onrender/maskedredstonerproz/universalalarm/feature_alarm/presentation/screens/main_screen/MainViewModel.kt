package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.screens.main_screen

import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.PeriodicWorkRequestBuilder
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.AlarmUseCases
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.Constants
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.framework.workers.AlarmActivePeriodLoopWorker
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Event
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.printException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit

/**
 * The [ViewModel] for the main screen
 * @param alarmUseCases The instance of [Alarm] related use cases
 * @param dispatcher The coroutine dispatcher instance for the [ViewModel]
 * @author MaskedRedstonerProZ
 */
class MainViewModel(
    private val alarmUseCases: AlarmUseCases,
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    /**
     * The state that contains the list of alarms in the database
     * @author MaskedRedstonerProZ
     */
    var alarmsListState by mutableStateOf<List<Alarm>>(emptyList())
        private set

    /**
     * The state that contains info weather the new alarm dialog should be shown or not
     * @author MaskedRedstonerProZ
     */
    var shouldShowNewAlarmDialogState by mutableStateOf(false)
        private set

    /**
     * The state that contains info weather the Loading progress bar should be shown
     * @author MaskedRedstonerProZ
     */
    var shouldShowAlarmLoadingProgressBarState by mutableStateOf(false)
        private set

    /**
     * The state that contains the alarm label text
     * @author MaskedRedstonerProZ
     */
    var alarmLabelState by mutableStateOf("")
        private set

    /**
     * The state that contains the due time of the alarm
     * @author MaskedRedstonerProZ
     */
    private var alarmDueTimeState: LocalTime by mutableStateOf(LocalTime.NOON)

    /**
     * The state that contains the due time of the alarm, formatted as a string
     * @author MaskedRedstonerProZ
     */
    val alarmDueTimeStringState: String by derivedStateOf {
        DateTimeFormatter
            .ofPattern("hh:mm a")
            .format(alarmDueTimeState)
    }

    /**
     * The state that contains a map with the Active periods of the alarm and the weekdays on which the alarm should trigger
     * @author MaskedRedstonerProZ
     */
    var activePeriodsAndWeekdaysMapState by mutableStateOf<Map<Pair<LocalDate, LocalDate>, Array<String>>>(
        emptyMap()
    )
        private  set

    /**
     * The flow that contains any error(s) which might occur when adding or deleting an [Alarm]
     * @author MaskedRedstonerProZ
     */
    private val _errorFlow = MutableSharedFlow<Exception>()

    /**
     * The flow that contains any error(s) which might occur when adding or deleting an [Alarm]
     * @author MaskedRedstonerProZ
     */
    val errorFlow = _errorFlow.asSharedFlow()

    /**
     * Event handler
     * @author MaskedRedstonerProZ
     */
    fun onEvent(e: Event) {
        when (e) {
            Event.FetchAlarmsFromDatabase -> {
                shouldShowAlarmLoadingProgressBarState = true
                viewModelScope.launch(dispatcher) {
                    alarmUseCases.getAllAlarms().collectLatest {
                        withContext(
                            if(dispatcher != Dispatchers.IO) {
                                dispatcher
                            } else {
                                Dispatchers.Main
                            }
                        ) {
                            alarmsListState = it
                            shouldShowAlarmLoadingProgressBarState = false
                        }
                    }
                }.invokeOnCompletion {
                    shouldShowAlarmLoadingProgressBarState = false
                }
            }

            Event.ShowCreateNewAlarmDialog -> {
                shouldShowNewAlarmDialogState = true
            }

            Event.HideCreateNewAlarmDialog -> {
                shouldShowNewAlarmDialogState = false
            }

            is Event.UpdateAlarmLabel -> {
                alarmLabelState = e.value
            }

            is Event.UpdateAlarmDueTime -> {
                alarmDueTimeState = e.value
            }

            is Event.UpdateActivePeriodsAndWeekdaysMap -> {
                activePeriodsAndWeekdaysMapState = activePeriodsAndWeekdaysMapState.plus(e.value)
            }

            Event.AddAlarmToDatabase -> {

                val alarmToAdd = Alarm(
                    (alarmsListState.size + 1).toLong(),
                    alarmLabelState,
                    LocalDateTime.of(
                        LocalDate.now(), LocalTime.parse(
                            alarmDueTimeStringState,
                            DateTimeFormatter.ofPattern("hh:mm a")
                        )
                    ),
                    activePeriodsAndWeekdaysMapState
                )
                viewModelScope.launch(dispatcher) {
                    try {
                        alarmUseCases.addAlarm(
                            alarmToAdd
                        )
                    } catch (e: Exception) {
                        printException(e)
                        _errorFlow.emit(e)
                    }
                }.invokeOnCompletion {
                    val newList = alarmsListState.toMutableStateList()

                    newList.add(alarmToAdd)

                    alarmsListState = newList.toList()
                    onEvent(Event.ScheduleAlarm(newList.lastIndex))
                    onEvent(Event.StartRescheduleWork)
                }
            }

            is Event.DeleteAlarmFromDatabase -> {
                viewModelScope.launch(dispatcher) {
                    try {
                        alarmUseCases.deleteAlarm(
                            alarmsListState[e.index].id
                        )
                    } catch (e: Exception) {
                        printException(e)
                        _errorFlow.emit(e)
                    }
                }
            }

            is Event.ScheduleAlarm -> {
                alarmUseCases.scheduleAlarm(alarmsListState[e.index])
            }

            is Event.CancelAlarm -> {
                alarmUseCases.cancelAlarm(alarmsListState[e.index])
            }

            Event.StartRescheduleWork -> {

                val activePeriodLoopWorkRequest = PeriodicWorkRequestBuilder<AlarmActivePeriodLoopWorker>(1, TimeUnit.DAYS)
                    .build()

                alarmUseCases.startWork(
                    activePeriodLoopWorkRequest,
                    Constants.ALARM_ACTIVE_PERIOD_LOOP_WORK_NAME
                )
            }

            Event.StopRescheduleWork -> {
                alarmUseCases.cancelWork(Constants.ALARM_ACTIVE_PERIOD_LOOP_WORK_NAME)
            }
        }
    }
}