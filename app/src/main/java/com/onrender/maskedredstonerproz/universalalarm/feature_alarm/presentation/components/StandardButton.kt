package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip

/**
 * The button used throughout the project
 * @param text The text of the button,
 * @param modifier The button's modifier
 * @param onClick The button's click event handler
 * @author MaskedRedstonerProZ
 */
@Composable
fun StandardButton(
    text: String,
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
) {
    Column {
        Button(
            modifier = modifier
                .clip(MaterialTheme.shapes.medium)
                .fillMaxWidth(),
            onClick = onClick
        ) {
            Text(
                text = text,
                color = MaterialTheme.colors.onPrimary
            )
        }
    }
}