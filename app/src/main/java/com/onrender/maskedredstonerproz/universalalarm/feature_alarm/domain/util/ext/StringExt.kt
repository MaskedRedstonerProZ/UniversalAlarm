package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.ext

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.Constants
import java.time.DayOfWeek
import java.time.LocalDate

/**
 * Takes the name of a weekday, and runs a given [action] on each occurrence of said weekday, within a period between [startDate] and [endDate]
 * @param startDate The start of the period.
 * @param endDate The end of the period.
 * @param action The action to run
 * @author MaskedRedstonerProZ
 */
fun String.onUpcomingWeekdayOccurrences(startDate: LocalDate, endDate: LocalDate, action: (LocalDate) -> Unit) {
    val weekdays = Constants.WEEKDAYS.split(',').map {
        it.trim().uppercase()
    }

    var currentDate = startDate

    while (currentDate.isBefore(endDate)) {
        if (currentDate.dayOfWeek == DayOfWeek.valueOf(weekdays.find {day -> day.startsWith(this.uppercase())  }!!)) {
            action(currentDate)
            currentDate = currentDate.plusDays(7)
            continue
        }

        currentDate = currentDate.plusDays(1)
    }
}