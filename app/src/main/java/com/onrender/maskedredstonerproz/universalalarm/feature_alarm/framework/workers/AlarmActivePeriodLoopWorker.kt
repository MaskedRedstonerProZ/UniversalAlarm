package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.framework.workers

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.AlarmUseCases
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * The worker that goes through all the [Alarm]s, and checks if the last active period has ended for any of them,
 * then reschedules the alarm for the next time the active periods begin
 * @author MaskedRedstonerProZ
 */
class AlarmActivePeriodLoopWorker(
    context: Context,
    workerParams: WorkerParameters
): CoroutineWorker(context, workerParams), KoinComponent {

    /**
     * The [AlarmUseCases] collection instance
     * @author MaskedRedstonerProZ
     */
    private val alarmUseCases: AlarmUseCases by inject()

    override suspend fun doWork(): Result {
        withContext(Dispatchers.IO) {
            alarmUseCases.getAllAlarms().collectLatest { alarms ->
                alarms.forEach { alarm ->
                    alarm.activePeriodsAndWeekdays.keys.last().let { (_, endDate) ->
                        if(LocalDate.now() == endDate.plusDays(1)) {

                            alarmUseCases.scheduleAlarm(alarm.copy(
                                dueTime = LocalDateTime.of(alarm.dueTime.toLocalDate().plusYears(1), alarm.dueTime.toLocalTime())
                            ))
                        }
                    }
                }
            }
        }
        return Result.success()
    }

}