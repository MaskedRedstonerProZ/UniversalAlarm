package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.Constants
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.*

/**
 * Thrown to indicate the label of the [Alarm]  has overflowed the limit
 * @param length The overflowing length of the label
 * @author MaskedRedstonerProZ
 */
class AlarmLabelLengthOverflowException(
    length: Int
): Exception("The label is ${length - Constants.MAX_ALARM_LABEL_LENGTH} ${if(length - Constants.MAX_ALARM_LABEL_LENGTH == 1) "character" else "characters"} longer than the limit, which is ${Constants.MAX_ALARM_LABEL_LENGTH}.")