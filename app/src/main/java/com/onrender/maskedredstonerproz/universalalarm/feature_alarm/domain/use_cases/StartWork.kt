package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkRequest
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.WorkManagerApi

/**
 * The use case for starting a [WorkRequest]
 * @param workManager The manager for the [WorkRequest]s
 * @author MaskedRedstonerProZ
 */
class StartWork(
    private val workManager: WorkManagerApi
) {

    operator fun invoke(workRequest: PeriodicWorkRequest, workName: String) {
        workManager.startWork(workRequest, workName, ExistingPeriodicWorkPolicy.KEEP)
    }

}
