package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.DrawerState
import androidx.compose.material.DrawerValue
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ModalDrawer
import androidx.compose.material.Text
import androidx.compose.material.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.AlarmUseCases
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components.StandardAppBar
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.dialogs.AddNewAlarmDialog
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.screens.about_screen.AboutScreen
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.screens.main_screen.MainScreen
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.screens.main_screen.MainViewModel
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.datetime.date.datepicker
import com.vanpra.composematerialdialogs.datetime.time.timepicker
import com.vanpra.composematerialdialogs.rememberMaterialDialogState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent.inject
import java.time.LocalDate
import java.time.LocalTime

/**
 * The navigation drawer
 * @param navController The navigation controller
 * @param drawerState The navigation drawer state
 * @author MaskedRedstonerProZ
 */
@Composable
fun Navigation(
	navController: NavHostController,
	drawerState: DrawerState
) {

	/**
	 * The coroutine scope used by the navigation drawer
	 * @author MaskedRedstonerProZ
	 */
	val scope = rememberCoroutineScope()
	val drawerShape = MaterialTheme.shapes.medium.copy(
		topStart = CornerSize(0.dp),
		bottomStart = CornerSize(0.dp)
	)

	ModalDrawer(
		drawerContent = {

			Spacer(modifier = Modifier.height(5.dp))

			arrayOf(
				Screen.MainScreen,
				Screen.AboutScreen
			).forEach {

				Row(
					modifier = Modifier
						.fillMaxWidth()
						.padding(start = 5.dp, end = 5.dp, top = 5.dp)
						.clip(MaterialTheme.shapes.medium)
						.background(MaterialTheme.colors.primary)
						.clickable {
							if (navController.currentDestination?.route != it()) {
								navController.apply {
									navigate(it())

									scope.launch {
										drawerState.close()
									}

									return@clickable
								}
							}

							scope.launch {
								drawerState.close()
							}
						}
				) {
					Text(
						text = "- ${it.name}",
						color = MaterialTheme.colors.onPrimary,
						modifier = Modifier.padding(start = 5.dp)
					)
				}
			}
		},
		drawerShape = drawerShape,
		scrimColor = Color.Transparent,
		drawerState = drawerState
	) {

		StandardAppBar(
			scope = scope,
			drawerState = drawerState
		)

		NavHost(
			navController = navController,
			startDestination = Screen.MainScreen()
		) {

			composable(Screen.MainScreen()) {

				val alarmUseCases: AlarmUseCases by inject(AlarmUseCases::class.java)

				val viewModel: MainViewModel = viewModel(factory = viewModelFactory {
					initializer {
						MainViewModel(
							alarmUseCases = alarmUseCases,
							dispatcher = Dispatchers.IO
						)
					}
				})

				val defaultDatePair = LocalDate.now() to LocalDate.now().plusDays(7)

				val dateDialogState = rememberMaterialDialogState()

				val timeDialogState = rememberMaterialDialogState()

				var isBeginning: Boolean by remember {
					mutableStateOf(true)
				}

				val activePeriods = remember {
					mutableStateListOf(
						defaultDatePair
					)
				}

				val weekdays = remember {
					mutableStateListOf<String>()
				}

				var oldPeriod: Pair<LocalDate, LocalDate> by remember {
					mutableStateOf(
						LocalDate.now() to LocalDate.now().plusDays(7)
					)
				}

				var currentPeriod: Pair<LocalDate, LocalDate> by remember {
					mutableStateOf(
						LocalDate.now() to LocalDate.now().plusDays(7)
					)
				}

				var selectedTime: LocalTime by remember {
					mutableStateOf(LocalTime.NOON)
				}

				MaterialDialog(
					dialogState = dateDialogState,
					buttons = {
						positiveButton(text = "Ok") {
							activePeriods[activePeriods.lastIndex] = currentPeriod
						}
						negativeButton(text = "Cancel") {
							currentPeriod = oldPeriod
							activePeriods[activePeriods.lastIndex] = oldPeriod
						}
					}
				) {
					datepicker(
						initialDate = if (isBeginning) LocalDate.now() else currentPeriod.first.plusDays(
							7
						),
						title = "Pick a date",
						allowedDateValidator = {
							isBeginning && !it.isBefore(LocalDate.now()) || !isBeginning && !it.isBefore(
								currentPeriod.first.plusDays(7)
							)
						}
					) {
						if (isBeginning) {
							oldPeriod = currentPeriod
							currentPeriod = it to currentPeriod.second
							return@datepicker
						}

						oldPeriod = currentPeriod
						currentPeriod = currentPeriod.first to it
					}

				}

				MaterialDialog(
					dialogState = timeDialogState,
					buttons = {
						positiveButton(text = "Ok") {
							viewModel.onEvent(Event.UpdateAlarmDueTime(selectedTime))
						}
						negativeButton(text = "Cancel") {
							selectedTime = LocalTime.NOON
						}
					}
				) {
					timepicker(
						initialTime = selectedTime,
						title = "Pick a time"
					) {
						selectedTime = it
					}
				}

				if (viewModel.shouldShowNewAlarmDialogState) {
					AddNewAlarmDialog(
						alarmLabel = viewModel.alarmLabelState,
						alarmDueTimeString = viewModel.alarmDueTimeStringState,
						activePeriods = activePeriods,
						onAlarmLabelChanged = {
							viewModel.onEvent(Event.UpdateAlarmLabel(it))
						},
						onAlarmDueTimeSelectionDialogShown = {
							timeDialogState.show()
						},
						onActivePeriodSelectionDialogShown = {
							isBeginning = it
							dateDialogState.show()
						},
						onActivePeriodAdded = {
							viewModel.onEvent(
								Event.UpdateActivePeriodsAndWeekdaysMap(
									mapOf(
										activePeriods[activePeriods.lastIndex] to weekdays.toList()
											.toTypedArray()
									)
								)
							)
							activePeriods.add(defaultDatePair)
						},
						onWeekdayAdded = {
							weekdays.add(it)
						},
						onWeekdayRemoved = {
							weekdays.remove(it)
						},
						onDismiss = {
							viewModel.onEvent(Event.HideCreateNewAlarmDialog)
						},
						onAlarmCreated = {
							if(!viewModel.activePeriodsAndWeekdaysMapState.containsKey(currentPeriod)) {
								viewModel.onEvent(
									Event.UpdateActivePeriodsAndWeekdaysMap(
										mapOf(
											activePeriods[activePeriods.lastIndex] to weekdays.toList()
												.toTypedArray()
										)
									)
								)
							}

							viewModel.onEvent(Event.AddAlarmToDatabase)
							viewModel.onEvent(Event.HideCreateNewAlarmDialog)
						}
					)
				}

				MainScreen(
					alarmsList = viewModel.alarmsListState,
					shouldShowAlarmLoadingProgressBar = viewModel.shouldShowAlarmLoadingProgressBarState,
					eventHandler = viewModel::onEvent
				)
			}

			composable(Screen.AboutScreen()) {
				AboutScreen()
			}

		}
	}
}

@Preview
@Composable
private fun NavigationPreview() {
	UniversalAlarmTheme {
		Navigation(
			navController = rememberNavController(),
			drawerState = rememberDrawerState(initialValue = DrawerValue.Open)
		)
	}
}