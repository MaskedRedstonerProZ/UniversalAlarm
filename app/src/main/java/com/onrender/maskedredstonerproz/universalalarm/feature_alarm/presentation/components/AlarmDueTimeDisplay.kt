package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.screens.main_screen.MainScreen
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.dialogs.AddNewAlarmDialog
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme

/**
 * The display for the alarm's dueTime, it is used in the alarm list in the [MainScreen],
 * as well as for the dueTime selector in the [AddNewAlarmDialog]
 * @param dueTime The actual time to display
 * @param variant The variant of the display
 * @param modifier The display's modifier
 * @author MaskedRedstonerProZ
 */
@Composable
fun AlarmDueTimeDisplay(
    dueTime: String,
    variant: AlarmDueTimeDisplayVariant,
    modifier: Modifier = Modifier,
) {

    Row(
        modifier = modifier.padding(0.dp),
        verticalAlignment = Alignment.Bottom
    ) {
        AutoResizedText(
            text = dueTime.dropLast(2),
            modifier = Modifier
                .padding(0.dp),
            style = if(variant == AlarmDueTimeDisplayVariant.SMALL) {
                MaterialTheme.typography.h2.copy(
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    )
                )
            } else {
                MaterialTheme.typography.h1.copy(
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false
                    )
                )
            },
            colour = MaterialTheme.colors.onSurface
        )
        AutoResizedText(
            text = dueTime.takeLast(2),
            style = if(variant == AlarmDueTimeDisplayVariant.SMALL) {
                MaterialTheme.typography.caption
            } else {
                MaterialTheme.typography.h3
            },
            colour = MaterialTheme.colors.onSurface,
        )
    }
}

/**
 * The variant of the display
 * @author MaskedRedstonerProZ
 */
enum class AlarmDueTimeDisplayVariant {

    /**
     * The small variant, used in the alarm list in the [MainScreen]
     * @author MaskedRedstonerProZ
     */
    SMALL,

    /**
     * The big variant, used in the time selector of the [AddNewAlarmDialog]
     * @author MaskedRedstonerProZ
     */
    BIG
}

@Preview
@Composable
private fun AlarmDueTimeDisplayPreviewVariantSmall() {
    UniversalAlarmTheme {
        AlarmDueTimeDisplay(dueTime = "00:00 pm", variant = AlarmDueTimeDisplayVariant.SMALL)
    }
}

@Preview
@Composable
private fun AlarmDueTimeDisplayPreviewVariantBIg() {
    UniversalAlarmTheme {
        AlarmDueTimeDisplay(dueTime = "00:00 am", variant = AlarmDueTimeDisplayVariant.BIG)
    }
}