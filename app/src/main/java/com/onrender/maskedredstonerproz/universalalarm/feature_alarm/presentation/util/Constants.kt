package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util

/**
 * The UI level constants
 * @author MaskedRedstonerProZ
 */
object Constants {

	const val EMAIL_TAG = "email"

}