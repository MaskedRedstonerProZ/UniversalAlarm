package com.onrender.maskedredstonerproz.universalalarm.util

/**
 * Global constants used in business logic as well as the ui
 * @author MaskedRedstonerProZ
 */
object Constants {

    const val DEBUG_TAG = "Universal_Alarm"

}