package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.screens.main_screen

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.FabPosition
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components.AlarmDueTimeDisplay
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components.AlarmDueTimeDisplayVariant
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Event
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.getCurrentPeriod
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Screen that contains the main functionality of this app
 * @param alarmsList The list of alarms in the database
 * @param shouldShowAlarmLoadingProgressBar if the alarm loading progress bar should be shown
 * @param eventHandler The handler for ui events such as a button being clicked, dialog being shown, etc.
 * @author MaskedRedstonerProZ
 */
@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MainScreen(
	alarmsList: List<Alarm>,
	shouldShowAlarmLoadingProgressBar: Boolean,
	eventHandler: (e: Event) -> Unit
) {

	if(alarmsList.isEmpty()) {
		eventHandler(Event.FetchAlarmsFromDatabase)
	}

	Scaffold(
		floatingActionButton = {
			UniversalAlarmTheme {
				FloatingActionButton(
					onClick = {
						eventHandler(
							Event.ShowCreateNewAlarmDialog
						)
					},
					modifier = Modifier
						.offset(y = (-65).dp),
					backgroundColor = MaterialTheme.colors.background
				) {
					Box(
						modifier = Modifier
							.padding(5.dp)
							.background(
								MaterialTheme.colors.surface,
								CircleShape
							)
					) {
						Icon(
							imageVector = Icons.Rounded.Add,
							contentDescription = "add new alarm",
							modifier = Modifier.size(50.dp)
						)
					}
				}
			}
		},
		floatingActionButtonPosition = FabPosition.Center,
		content = {
			Column(
				modifier = Modifier
					.fillMaxSize()
					.background(
						MaterialTheme.colors.background
					)
					.padding(
						start = 10.dp,
						end = 10.dp,
						top = if (shouldShowAlarmLoadingProgressBar) {
							10.dp
						} else {
							0.dp
						},
						bottom = 10.dp
					)
			) {
				LazyColumn {
					if (shouldShowAlarmLoadingProgressBar) item {
						Row(
							modifier = Modifier
								.fillMaxWidth()
								.clip(MaterialTheme.shapes.medium)
								.background(
									color = MaterialTheme.colors.surface,
									shape = MaterialTheme.shapes.medium
								)
								.padding(),
							verticalAlignment = Alignment.CenterVertically,
							horizontalArrangement = Arrangement.Center
						) {
							CircularProgressIndicator(modifier = Modifier.padding(10.dp))
						}
					}
					items(
						items = alarmsList,
						itemContent = {
							if (it.activePeriodsAndWeekdays.getCurrentPeriod() != null) {
								Spacer(modifier = Modifier.height(10.dp))
								Row(
									modifier = Modifier
										.fillMaxWidth()
										.clip(MaterialTheme.shapes.medium)
										.background(
											color = MaterialTheme.colors.surface,
											shape = MaterialTheme.shapes.medium
										)
										.padding(end = 10.dp),
									verticalAlignment = Alignment.CenterVertically,
									horizontalArrangement = Arrangement.SpaceBetween
								) {
									Column(
										modifier = Modifier
											.padding(
												start = 10.dp,
												bottom = 10.dp
											)
									) {
										AlarmDueTimeDisplay(
											dueTime = it.dueTime.format(
												DateTimeFormatter.ofPattern("hh:mm a")
											),
											variant = AlarmDueTimeDisplayVariant.SMALL
										)
										Text(
											text = it.label,
											style = MaterialTheme.typography.caption
										)
									}
									Text(
										text = it.activePeriodsAndWeekdays[
											it.activePeriodsAndWeekdays.getCurrentPeriod()
										]?.joinToString(
											separator = " "
										) ?: ""
									)
								}
							}
						}
					)
				}
			}
		},
	)

}

@Preview
@Composable
private fun MainScreenPreview() {
	UniversalAlarmTheme {
		MainScreen(
			alarmsList = listOf(
				Alarm(
					id = 0.toLong(),
					label = "Lorem ipsum",
					dueTime = LocalDateTime.now(),
					activePeriodsAndWeekdays = mapOf(
						LocalDate.now() to LocalDate.now().plusDays(5) to arrayOf("M", "T", "W")
					)
				),
				Alarm(
					id = 1.toLong(),
					label = "Lorem ipsum",
					dueTime = LocalDateTime.now(),
					activePeriodsAndWeekdays = mapOf(
						LocalDate.now() to LocalDate.now().plusDays(5) to arrayOf("M T W TH F S SU")
					)
				),
				Alarm(
					id = 2.toLong(),
					label = "Lorem ipsum",
					dueTime = LocalDateTime.now(),
					activePeriodsAndWeekdays = mapOf(
						LocalDate.now() to LocalDate.now().plusDays(5) to arrayOf("M", "T", "W")
					)
				),
				Alarm(
					id = 3.toLong(),
					label = "Lorem ipsum",
					dueTime = LocalDateTime.now(),
					activePeriodsAndWeekdays = mapOf(
						LocalDate.now() to LocalDate.now().plusDays(5) to arrayOf("M", "T", "W")
					)
				),
				Alarm(
					id = 4.toLong(),
					label = "Lorem ipsum",
					dueTime = LocalDateTime.now(),
					activePeriodsAndWeekdays = mapOf(
						LocalDate.now() to LocalDate.now().plusDays(5) to arrayOf("M", "T", "W")
					)
				),
				Alarm(
					id = 5.toLong(),
					label = "Lorem ipsum",
					dueTime = LocalDateTime.now(),
					activePeriodsAndWeekdays = mapOf(
						LocalDate.now() to LocalDate.now().plusDays(5) to arrayOf("M", "T", "W")
					)
				),
				Alarm(
					id = 6.toLong(),
					label = "Lorem ipsum",
					dueTime = LocalDateTime.now(),
					activePeriodsAndWeekdays = mapOf(
						LocalDate.now() to LocalDate.now().plusDays(5) to arrayOf("M", "T", "W")
					)
				),
				Alarm(
					id = 7.toLong(),
					label = "Lorem ipsum",
					dueTime = LocalDateTime.now(),
					activePeriodsAndWeekdays = mapOf(
						LocalDate.now() to LocalDate.now().plusDays(5) to arrayOf("M", "T", "W")
					)
				)
			),
			shouldShowAlarmLoadingProgressBar = true,
			eventHandler = {
				println(it)
			}
		)
	}
}