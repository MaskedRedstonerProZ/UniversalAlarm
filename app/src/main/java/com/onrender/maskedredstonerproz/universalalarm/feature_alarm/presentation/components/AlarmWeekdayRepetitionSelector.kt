package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme

 /**
  * The selector which determines what days the alarm should ring
  * @param selected The list of selected days
  * @param modifier The selector's modifier
  * @param onWeekdayAddedToSelection The weekday selection event handler
  * @param onWeekdayRemovedFromSelection The weekday deselection event handler
  * @author MaskedRedstonerProZ
  */
@Composable
fun AlarmWeekdayRepetitionSelector(
    selected: List<String>,
    modifier: Modifier = Modifier,
    onWeekdayAddedToSelection: (it: String) -> Unit,
    onWeekdayRemovedFromSelection: (it: String) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 5.dp),
        horizontalArrangement = Arrangement.Center
    ) {
        val weekdays = "M T W TH F S SU".split(" ")

        @Composable
        fun calculateShape(i: Int): Shape {
            var shape = MaterialTheme.shapes.medium

            if (i != 0 && selected.contains(weekdays[i - 1])) {
                shape = shape.copy(
                    topStart = CornerSize(0.dp),
                    bottomStart = CornerSize(0.dp)
                )
            }

            if (i != weekdays.lastIndex && selected.contains(weekdays[i + 1])) {
                shape = shape.copy(
                    topEnd = CornerSize(0.dp),
                    bottomEnd = CornerSize(0.dp)
                )
            }

            return shape
        }
        weekdays.forEachIndexed { i, weekday ->
            AutoResizedText(
                text = weekday,
                style = MaterialTheme.typography.h2.copy(
                    fontSize = 40.sp,
                    color = MaterialTheme.colors.onSurface
                ),
                modifier = modifier
                    .background(
                        color = if (selected.contains(weekday)) Color.Black else Color.Transparent,
                        shape = calculateShape(i)
                    )
                    .padding(horizontal = 2.5.dp)
                    .clickable {
                        if (selected.contains(weekday)) {
                            onWeekdayRemovedFromSelection(weekday)
                        } else {
                            onWeekdayAddedToSelection(weekday)
                        }
                    }
            )
        }
    }
}

@Preview
@Composable
fun AlarmWeekdayRepetitionSelectorPreview() {
    UniversalAlarmTheme {

        val selected = remember { mutableStateListOf<String>() }

        AlarmWeekdayRepetitionSelector(
            selected = selected,
            onWeekdayAddedToSelection = selected::add,
            onWeekdayRemovedFromSelection = selected::remove
        )
    }
}