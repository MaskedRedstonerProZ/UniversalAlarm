package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api

import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager

/**
 * The manager api for interacting with [WorkManager]
 * @author MaskedRedstonerProZ
 */
interface WorkManagerApi {

    fun startWork(workRequest: OneTimeWorkRequest, workName: String, workPolicy: ExistingWorkPolicy)

    fun startWork(workRequest: PeriodicWorkRequest, workName: String, workPolicy: ExistingPeriodicWorkPolicy)

    fun cancelWork(workName: String)

}