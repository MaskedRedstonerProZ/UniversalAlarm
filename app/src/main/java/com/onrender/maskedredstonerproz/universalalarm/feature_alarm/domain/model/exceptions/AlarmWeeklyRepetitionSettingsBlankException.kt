package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm

/**
 * Thrown to indicate the weekday repetition settings of the [Alarm] are blank
 * @author MaskedRedstonerProZ
 */
class AlarmWeeklyRepetitionSettingsBlankException: Exception("The weekly repetition settings can't be blank")