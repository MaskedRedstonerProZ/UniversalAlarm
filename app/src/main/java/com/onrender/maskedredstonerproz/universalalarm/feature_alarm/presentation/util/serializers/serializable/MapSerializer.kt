package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.serializers.serializable

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import java.time.LocalDate

/**
 * Custom serializer for map
 * @author MaskedRedstonerProZ
 */
@Serializable
data class MapSerializer(
	val map: Map<Pair<@Contextual LocalDate, @Contextual LocalDate>, Array<String>>
)
