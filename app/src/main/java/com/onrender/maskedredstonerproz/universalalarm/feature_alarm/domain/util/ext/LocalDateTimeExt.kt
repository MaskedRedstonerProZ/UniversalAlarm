package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.ext

import java.time.Duration
import java.time.LocalDateTime

/**
 * Gets the duration between the current moment, and a given [LocalDateTime] instance, in milliseconds
 * @author MaskedRedstonerProZ
 */
fun LocalDateTime.getTimeDifferenceInMillis(): Long {
	val currentDateTime = LocalDateTime.now()
	val duration = Duration.between(currentDateTime, this)
	return duration.toMillis()
}