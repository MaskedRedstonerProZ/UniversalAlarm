package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api

import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.WorkManagerApi

/**
 * Android implementation of the [WorkManagerApi]
 * @author MaskedRedstonerProZ
 */
class AndroidWorkManagerApi(
    private val workManager: WorkManager,
) : WorkManagerApi {

    override fun startWork(
        workRequest: OneTimeWorkRequest,
        workName: String,
        workPolicy: ExistingWorkPolicy
    ) {
        workManager.enqueueUniqueWork(workName, workPolicy, workRequest)
    }

    override fun startWork(
        workRequest: PeriodicWorkRequest,
        workName: String,
        workPolicy: ExistingPeriodicWorkPolicy
    ) {
        workManager.enqueueUniquePeriodicWork(workName, workPolicy, workRequest)
    }

    override fun cancelWork(
        workName: String
    ) {
        workManager.cancelUniqueWork(workName)
    }
}