package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.framework.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.provider.Settings
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.printException
import java.io.IOException

/**
 * The [BroadcastReceiver] which handles [Alarm] broadcasts
 * @author MaskedRedstonerProZ
 */
class AlarmReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
//        val player = MediaPlayer.create(context, Settings.System.DEFAULT_ALARM_ALERT_URI)
//        player.start()

        val mediaPlayerScan = MediaPlayer()
        try {
        	mediaPlayerScan.setDataSource(context!!, Settings.System.DEFAULT_ALARM_ALERT_URI)

            mediaPlayerScan.setAudioAttributes(
                AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build()
            )

            mediaPlayerScan.prepare()
        } catch (e: IOException) {
            printException(e)
        }
        mediaPlayerScan.start()
    }
}