package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.AlarmSchedulerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.ext.getTimeDifferenceInMillis
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.framework.receivers.AlarmReceiver
import com.onrender.maskedredstonerproz.universalalarm.util.Constants.DEBUG_TAG

/**
 * Android Implementation of the [AlarmSchedulerApi]
 * @author MaskedRedstonerProZ
 */
class AndroidAlarmSchedulerApi(
    private val context: Context,
    private val alarmManager: AlarmManager
): AlarmSchedulerApi {

    @Suppress("MissingPermission")
    override fun schedule(alarm: Alarm) {

        Log.d(DEBUG_TAG, alarm.dueTime.getTimeDifferenceInMillis().toString())

        alarmManager.setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP,
            alarm.dueTime.getTimeDifferenceInMillis(),
            PendingIntent.getBroadcast(
                context,
                alarm.hashCode(),
                Intent(context, AlarmReceiver::class.java),
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
        )
    }

    override fun cancel(alarm: Alarm) {
        alarmManager.cancel(
            PendingIntent.getBroadcast(
                context,
                alarm.hashCode(),
                Intent(context, AlarmReceiver::class.java),
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
        )
    }
}