package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import java.time.LocalDate

/**
 * Thrown to indicate the startDate of the [Alarm]'s active period is set to a one after the endDate
 * @param startDate The starting date of the [Alarm]'s active period
 * @param endDate The ending date of the [Alarm]'s active period
 * @author MaskedRedstonerProZ
 */
class AlarmActivePeriodStartAfterEndException(
    startDate: LocalDate,
    endDate: LocalDate
): Exception("The chosen start date for the active period (${startDate}) happens to be after the chosen end date ($endDate).")