package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.ext.onUpcomingWeekdayOccurrences
import java.time.LocalDate

/**
 * The weekday list
 * @author MaskedRedstonerProZ
 */
private val weekdays = Constants.WEEKDAYS.split(',').map {
    it.trim().uppercase()
}

/**
 * Calculates the due date of a given alarm from the due time and weekly repetition settings, then runs a given action based on the date
 * @param alarm The [Alarm] itself.
 * @param action The action to run.
 * @author MaskedRedstonerProZ
 */
fun runBasedOnDueDateOf(alarm: Alarm, action: (date: LocalDate) -> Unit) {
    val activePeriods = alarm.activePeriodsAndWeekdays.keys.toTypedArray()

    val weeklyRepetitionSettings = alarm.activePeriodsAndWeekdays.values.toTypedArray()

    activePeriods.forEachIndexed { index, (startDate, endDate) ->
        val weeklyRepetitionSettingsForPeriod = weeklyRepetitionSettings[index]

        weeklyRepetitionSettingsForPeriod.forEach {

            weekdays.find { day -> day.startsWith(it) }!!.onUpcomingWeekdayOccurrences(
                if(LocalDate.now().year == startDate.year) LocalDate.now() else startDate,
                endDate
            ) { date ->
                action(date)
            }
        }

    }
}