package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.repository

import app.cash.sqldelight.coroutines.asFlow
import app.cash.sqldelight.coroutines.mapToList
import com.onrender.maskedredstonerproz.universalalarm.AlarmDatabase
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.repository.AlarmRepository
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.serializers.LocalDateSerializer
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.serializers.StringArraySerializer
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.serializers.serializable.MapSerializer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset

/**
 * The [AlarmRepository] production implementation
 * @author MaskedRedstonerProZ
 */
class AlarmRepositoryImpl(
    db: AlarmDatabase
) : AlarmRepository {

    private val queries = db.alarmEntityQueries

    private val json = Json {
        serializersModule = SerializersModule {
            contextual(LocalDate::class, LocalDateSerializer)
            contextual(Array<String>::class, StringArraySerializer)
        }
        allowStructuredMapKeys = true
    }

    override suspend fun getAlarmById(id: Long): Alarm? {
        val entity = withContext(Dispatchers.IO) {
            queries.getAlarmById(id).executeAsOneOrNull()
        }

        entity.apply {
            if (this == null) return null

            return Alarm(
                id,
                label,
                LocalDateTime.ofEpochSecond(dueTime, 0, ZoneOffset.UTC),
                json.decodeFromString(MapSerializer.serializer(), activePeriodAndWeekdayMapJson).map
            )
        }
    }

    override fun getAllAlarms(): Flow<List<Alarm>> {
        return queries.getAllAlarms().asFlow().mapToList(Dispatchers.IO).map {
            it.map { entity ->
                Alarm(
                    entity.id,
                    entity.label,
                    LocalDateTime.ofEpochSecond(entity.dueTime, 0, ZoneOffset.UTC),
                    json.decodeFromString(MapSerializer.serializer(), entity.activePeriodAndWeekdayMapJson).map
                )
            }
        }
    }

    override suspend fun deleteAlarmById(id: Long) {
        withContext(Dispatchers.IO) {
            queries.deleteAlarmById(id)
        }
    }

    override suspend fun insertAlarm(
        label: String,
        dueTime: LocalDateTime,
        activePeriodAndWeekDayMap: Map<Pair<LocalDate, LocalDate>, Array<String>>
    ) {
        withContext(Dispatchers.IO) {
            queries.insertAlarm(label, dueTime.toEpochSecond(ZoneOffset.UTC), json.encodeToString(MapSerializer.serializer(), MapSerializer(activePeriodAndWeekDayMap)))
        }
    }
}