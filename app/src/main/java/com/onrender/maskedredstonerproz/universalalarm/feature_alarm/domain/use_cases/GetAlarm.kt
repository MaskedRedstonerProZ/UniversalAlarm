package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.repository.AlarmRepository
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions.AlarmIdNonexistentException
import kotlin.jvm.Throws

/**
 * The use case for adding an [Alarm]
 * @param repository The [AlarmRepository] for accessing the database
 * @throws AlarmIdNonexistentException If there is no [Alarm] with the given id
 * @author MaskedRedstonerProZ
 */
class GetAlarm(
    private val repository: AlarmRepository
) {

    @Throws(AlarmIdNonexistentException::class)
    // invocation operator
    suspend operator fun invoke(id: Long): Alarm {

        return repository.getAlarmById(id) ?: throw AlarmIdNonexistentException(id)
    }

}