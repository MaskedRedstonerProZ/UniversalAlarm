package com.onrender.maskedredstonerproz.universalalarm.di

import android.app.AlarmManager
import androidx.work.WorkManager
import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.android.AndroidSqliteDriver
import com.onrender.maskedredstonerproz.universalalarm.AlarmDatabase
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api.AndroidAlarmSchedulerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api.AndroidWorkManagerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.repository.AlarmRepository
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.AlarmSchedulerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.WorkManagerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.repository.AlarmRepositoryImpl
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.AddAlarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.AlarmUseCases
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.CancelAlarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.CancelWork
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.DeleteAlarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.GetAlarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.GetAllAlarms
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.ScheduleAlarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.StartWork
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.Constants
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/**
 * Application module for dependency injection
 * @author MaskedRedstonerProZ
 */
val appModule = module {

    single {
        val driver: SqlDriver = AndroidSqliteDriver(AlarmDatabase.Schema, androidContext(), Constants.DATABASE_FILE_NAME)
        AlarmDatabase(driver)
    }

    single<AlarmRepository> {
        AlarmRepositoryImpl(get())
    }

    single<AlarmSchedulerApi> {
        val alarmManager = androidContext().getSystemService(AlarmManager::class.java)

        AndroidAlarmSchedulerApi(androidContext(), alarmManager)
    }

    single<WorkManagerApi> {
        val workManager: WorkManager = WorkManager.getInstance(androidContext())

        AndroidWorkManagerApi(workManager)
    }

    single {
        AlarmUseCases(
            getAlarm = GetAlarm(get()),
            getAllAlarms = GetAllAlarms(get()),
            deleteAlarm = DeleteAlarm(get()),
            addAlarm = AddAlarm(get()),
            scheduleAlarm = ScheduleAlarm(get()),
            cancelAlarm = CancelAlarm(get()) ,
            startWork = StartWork(get()),
            cancelWork = CancelWork(get())
        )
    }

}