package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme

/**
 * The text field used throughout the project
 * @param value The value of the text field
 * @param modifier The text field's modifier
 * @param placeholder The placeholder of the text field
 * @param onValueChanged The value change callback
 * @author MaskedRedstonerProZ
 */
@Composable
fun StandardTextField(
    value: String,
    modifier: Modifier = Modifier,
    placeholder: @Composable () -> Unit,
    onValueChanged: (it: String) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clip(MaterialTheme.shapes.medium)
            .background(Color.Black)
            .padding(5.dp)
    ) {
        OutlinedTextField(
            value = value,
            onValueChange = onValueChanged,
            textStyle = MaterialTheme.typography.body1,
            placeholder = placeholder,
            singleLine = true,
            colors = TextFieldDefaults.outlinedTextFieldColors(
                textColor = MaterialTheme.colors.onSurface,
                unfocusedBorderColor = MaterialTheme.colors.primary
            ),
            shape = MaterialTheme.shapes.small,
            modifier = modifier
                .fillMaxWidth()
                .border(
                    2.5.dp,
                    Color.Red,
                    MaterialTheme.shapes.small
                )
        )
    }
}

@Preview
@Composable
private fun StandardTextFieldPreview() {
    UniversalAlarmTheme {

        var value: String by remember {
            mutableStateOf("")
        }

        StandardTextField(
            value = value,
            placeholder = {
                Text(text = "Test")
            },
            onValueChanged = {
                value = it
            }
        )
    }
}