package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.dialogs

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.onrender.maskedredstonerproz.universalalarm.R
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components.AlarmActivePeriodSelector
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components.AlarmDueTimeDisplay
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components.AlarmDueTimeDisplayVariant
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components.AlarmWeekdayRepetitionSelector
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components.StandardButton
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components.StandardTextField
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.datetime.date.datepicker
import com.vanpra.composematerialdialogs.rememberMaterialDialogState
import java.time.LocalDate

/**
 * The dialog for adding a new alarm
 * @param modifier The dialog's modifier
 * @param alarmLabel The label of the alarm, which the user uses to identify a given alarm
 * @param alarmDueTimeString The due time of the alarm, which determines when the alarm is supposed to ring
 * @param onAlarmLabelChanged The alarm label change callback
 * @param onAlarmDueTimeSelectionDialogShown The due time picker dialog callback
 * @param onActivePeriodSelectionDialogShown The active period date picker dialog callback
 * @param onActivePeriodAdded The active period record adding callback
 * @param onWeekdayAdded The weekday  adding callback
 * @param onWeekdayRemoved The weekday removing callback
 * @param onDismiss The dismissal callback of this dialog
 * @param onAlarmCreated The alarm creation callback of this dialog
 * @author MaskedRedstonerProZ
 */
@Composable
fun AddNewAlarmDialog(
	modifier: Modifier = Modifier,
	alarmLabel: String,
	alarmDueTimeString: String,
	activePeriods: List<Pair<LocalDate, LocalDate>>,
	onAlarmLabelChanged: (it: String) -> Unit,
	onAlarmDueTimeSelectionDialogShown: () -> Unit,
	onActivePeriodSelectionDialogShown: (beginning: Boolean) -> Unit,
	onActivePeriodAdded: () -> Unit,
	onWeekdayAdded: (it: String) -> Unit,
	onWeekdayRemoved: (it: String) -> Unit,
	onDismiss: () -> Unit,
	onAlarmCreated: () -> Unit
) {
	Dialog(onDismissRequest = onDismiss) {
		Column(
			modifier = modifier
				.fillMaxWidth()
				.clip(MaterialTheme.shapes.large)
				.background(
					MaterialTheme.colors.background
				)
				.padding(
					horizontal = 10.dp,
					vertical = 5.dp
				),
			horizontalAlignment = Alignment.CenterHorizontally,
			verticalArrangement = Arrangement.SpaceEvenly
		) {

			var shouldWeekdaySelectionBeReset by remember {
				mutableStateOf(false)
			}

			val selected = remember(shouldWeekdaySelectionBeReset) {
				mutableStateListOf<String>()
			}

			Row(
				modifier = Modifier
					.fillMaxWidth()
					.padding(bottom = 5.dp),
				horizontalArrangement = Arrangement.Start
			) {
				Text(
					text = stringResource(
						id = R.string.add_new_alarm_dialog_title
					),
					style = MaterialTheme.typography.caption.copy(
						color = MaterialTheme.colors.onSurface
					)
				)
			}
			Row(
				modifier = Modifier.fillMaxWidth(),
			) {
				StandardTextField(
					value = alarmLabel,
					placeholder = {
						Text(
							text = stringResource(
								id = R.string.alarm_label
							),
							style = MaterialTheme.typography.subtitle1
						)
					},
					onValueChanged = onAlarmLabelChanged
				)
			}
			AlarmDueTimeDisplay(
				dueTime = alarmDueTimeString,
				variant = AlarmDueTimeDisplayVariant.BIG,
				modifier = Modifier
					.clickable {
						onAlarmDueTimeSelectionDialogShown()
					}
			)

			AlarmWeekdayRepetitionSelector(
				selected = selected,
				onWeekdayAddedToSelection = {
					selected.add(it)
					onWeekdayAdded(it)
				},
				onWeekdayRemovedFromSelection = {
					selected.remove(it)
					onWeekdayRemoved(it)
				}
			)

			AlarmActivePeriodSelector(
				activePeriods = activePeriods,
				onActivePeriodAdded = onActivePeriodAdded,
				onActivePeriodSelectionDialogShown = onActivePeriodSelectionDialogShown,
				resetWeekdaySelection = {
					shouldWeekdaySelectionBeReset = !shouldWeekdaySelectionBeReset
				}
			)

			StandardButton(
				text = stringResource(
					id = R.string.add_alarm
				),
				onClick = onAlarmCreated
			)
		}
	}
}

@Preview
@Composable
private fun AddNewAlarmDialogPreview() {
	UniversalAlarmTheme {
		val dateDialogState = rememberMaterialDialogState()

		var isBeginning: Boolean by remember {
			mutableStateOf(true)
		}

		var shouldShowDialog: Boolean by remember {
			mutableStateOf(true)
		}

		var label: String by remember {
			mutableStateOf("")
		}

		val dueTimeString: String by remember {
			mutableStateOf("00: 00 am")
		}

		val activePeriods = remember {
			mutableStateListOf(
				LocalDate.now() to LocalDate.now().plusDays(7)
			)
		}

		val weekdays = remember {
			mutableStateListOf<String>()
		}

		var oldPeriod: Pair<LocalDate, LocalDate> by remember {
			mutableStateOf(
				LocalDate.now() to LocalDate.now().plusDays(7)
			)
		}

		var currentPeriod: Pair<LocalDate, LocalDate> by remember {
			mutableStateOf(
				LocalDate.now() to LocalDate.now().plusDays(7)
			)
		}

		MaterialDialog(
			dialogState = dateDialogState,
			buttons = {
				positiveButton(text = "Ok") {
					activePeriods[activePeriods.lastIndex] = currentPeriod
				}
				negativeButton(text = "Cancel") {
					currentPeriod = oldPeriod
					activePeriods[activePeriods.lastIndex] = oldPeriod
				}
			}
		) {
			datepicker(
				initialDate = if (isBeginning) LocalDate.now() else currentPeriod.first.plusDays(7),
				title = "Pick a date",
				allowedDateValidator = {
					isBeginning && !it.isBefore(LocalDate.now()) || !isBeginning && !it.isBefore(
						currentPeriod.first.plusDays(7)
					)
				}
			) {
				if (isBeginning) {
					oldPeriod = currentPeriod
					currentPeriod = it to currentPeriod.second
					return@datepicker
				}

				oldPeriod = currentPeriod
				currentPeriod = currentPeriod.first to it
			}

		}

		if (shouldShowDialog) {
			AddNewAlarmDialog(
				alarmLabel = label,
				alarmDueTimeString = dueTimeString,
				activePeriods = activePeriods,
				onAlarmLabelChanged = {
					label = it
				},
				onAlarmDueTimeSelectionDialogShown = {

				},
				onActivePeriodSelectionDialogShown = {
					isBeginning = it
					dateDialogState.show()
				},
				onActivePeriodAdded = {
					activePeriods.add(LocalDate.now() to LocalDate.now().plusDays(7))
				},
				onWeekdayAdded = {
					weekdays.add(it)
				},
				onWeekdayRemoved = {
					weekdays.remove(it)
				},
				onDismiss = {
					shouldShowDialog = false
				}
			) {
				shouldShowDialog = false
			}
		}
	}
}