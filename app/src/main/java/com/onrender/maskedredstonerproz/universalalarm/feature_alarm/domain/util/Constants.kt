package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm

/**
 * The business logic constants
 * @author MaskedRedstonerProZ
 */
object Constants {

    /**
     * The name of the database file
     * @author MaskedRedstonerProZ
     */
    const val DATABASE_FILE_NAME = "alarmDatabase.db"

    /**
     * The maximum length of every [Alarm] label
     * @author MaskedRedstonerProZ
     */
    const val MAX_ALARM_LABEL_LENGTH = 20

    /**
     * The weekdays
     * @author MaskedRedstonerProZ
     */
    const val WEEKDAYS = "Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday"

    /**
     * The [Alarm]'s active period looping work name
     * @author MaskedRedstonerProZ
     */
    const val ALARM_ACTIVE_PERIOD_LOOP_WORK_NAME = "alarm_active_period_loop_work_name"

}