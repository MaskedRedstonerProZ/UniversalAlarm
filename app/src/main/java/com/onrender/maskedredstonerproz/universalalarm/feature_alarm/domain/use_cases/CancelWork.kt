package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import androidx.work.WorkRequest
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.WorkManagerApi

/**
 * The use case for starting a [WorkRequest]
 * @param workManager The manager for the [WorkRequest]s
 * @author MaskedRedstonerProZ
 */
class CancelWork(
    private val workManager: WorkManagerApi
) {

    operator fun invoke(workName: String) {
        workManager.cancelWork(workName)
    }

}