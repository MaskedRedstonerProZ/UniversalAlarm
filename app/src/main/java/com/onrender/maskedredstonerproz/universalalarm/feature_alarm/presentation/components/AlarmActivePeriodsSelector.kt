package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.AddCircleOutline
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.onrender.maskedredstonerproz.universalalarm.R
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * The selector which determines what period(s) the alarm should ring
 * @param modifier The selector's modifier
 * @param activePeriods The list of selected periods
 * @param onActivePeriodAdded The period adding event handler
 * @param onActivePeriodSelectionDialogShown The period date selection dialog showing event handler
 * @param resetWeekdaySelection The weekday selection reset callback
 * @author MaskedRedstonerProZ
 */
@Composable
fun AlarmActivePeriodSelector(
	modifier: Modifier = Modifier,
	activePeriods: List<Pair<LocalDate, LocalDate>>,
	onActivePeriodAdded: () -> Unit,
	onActivePeriodSelectionDialogShown: (beginning: Boolean) -> Unit,
	resetWeekdaySelection: () -> Unit,
) {

	/**
	 * The period list mapped to contain an extra boolean track where the period adding button should be shown
	 * @author MaskedRedstonerProZ
	 */
	val activePeriodsList = remember {
		derivedStateOf {
			activePeriods.mapIndexed { i, it ->
				if(i == activePeriods.lastIndex) {
					it to true
				} else it to false
			}
		}
	}

	LazyColumn(
		modifier = modifier
	) {
		items(
			items = activePeriodsList.value
		) {

			val (item, shouldShowAddItem) = it

			Spacer(modifier = Modifier.height(10.dp))
			Row(
				modifier = Modifier
					.clip(
						MaterialTheme.shapes.medium
					)
					.background(
						color = MaterialTheme.colors.surface,
						shape = MaterialTheme.shapes.medium
					)
					.padding(2.5.dp),
				verticalAlignment = Alignment.CenterVertically
			) {
				Row {
					Text(
						text = item.first.format(
							DateTimeFormatter.ofPattern("dd-MM-yyyy")
						),
						style = MaterialTheme.typography.caption.copy(
							color = MaterialTheme.colors.onSurface
						),
						modifier = Modifier.clickable {

							if(!shouldShowAddItem) return@clickable

							onActivePeriodSelectionDialogShown(true)
						}
					)
					Text(
						text = "/",
						style = MaterialTheme.typography.caption.copy(
							color = MaterialTheme.colors.onSurface
						)
					)
					Text(
						text = item.second.format(
							DateTimeFormatter.ofPattern("dd-MM-yyyy")
						),
						style = MaterialTheme.typography.caption.copy(
							color = MaterialTheme.colors.onSurface
						),
						modifier = Modifier.clickable {

							if (!shouldShowAddItem) return@clickable

							onActivePeriodSelectionDialogShown(false)
						}
					)
				}

				if (shouldShowAddItem) {
					Image(
						imageVector = Icons.Outlined.AddCircleOutline,
						contentDescription = stringResource(
							id = R.string.add_new_active_period
						),
						colorFilter = ColorFilter.lighting(
							multiply = MaterialTheme.colors.onSurface,
							add = MaterialTheme.colors.onSurface
						),
						modifier = Modifier
							.clickable {
								onActivePeriodAdded()
								resetWeekdaySelection()
							}
					)
				}
			}
			if (shouldShowAddItem) Spacer(
				modifier = Modifier.height(
					10.dp
				)
			)
		}
	}
}

@Preview
@Composable
private fun AlarmActivePeriodSelectorPreview() {
	UniversalAlarmTheme {

		val defaultDatePair = LocalDate.now() to LocalDate.now().plusDays(7)

		val activePeriods = remember {
			mutableStateListOf(defaultDatePair)
		}

		AlarmActivePeriodSelector(
			activePeriods = activePeriods,
			onActivePeriodAdded = {
				activePeriods.add(defaultDatePair)
			},
			onActivePeriodSelectionDialogShown = {
				activePeriods[activePeriods.lastIndex] = LocalDate.now().plusDays(Math.random().toLong()) to LocalDate.now().plusDays(7 + Math.random().toLong())
			},
			resetWeekdaySelection = {}
		)

	}
}