package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.DrawerValue
import androidx.compose.material.rememberDrawerState
import androidx.navigation.compose.rememberNavController
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Navigation
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme

/**
 * Main Activity Class
 * @author MaskedRedstonerProZ
 */
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            UniversalAlarmTheme {
                Navigation(
                    navController = rememberNavController(),
                    drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
                )
            }
        }
    }
}