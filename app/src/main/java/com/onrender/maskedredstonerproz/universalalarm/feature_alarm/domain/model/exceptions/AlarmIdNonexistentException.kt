package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm

/**
 * Thrown to indicate the id of the [Alarm] does not exist
 * @param id The nonexistent id of the [Alarm]
 * @author MaskedRedstonerProZ
 */
class AlarmIdNonexistentException(id: Long): Exception("There is no such alarm with id $id.")