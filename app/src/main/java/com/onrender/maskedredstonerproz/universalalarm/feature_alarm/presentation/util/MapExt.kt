package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util

import java.time.LocalDate

/**
 * Gets the period the user is currently in
 * @author MaskedRedstonerProZ
 */
fun Map<Pair<LocalDate, LocalDate>, Array<String>>.getCurrentPeriod(): Pair<LocalDate, LocalDate>? {
    keys.forEach {
        if(LocalDate.now().isAfter(it.first) && LocalDate.now().isBefore(it.second) || (LocalDate.now() == it.first || LocalDate.now() == it.second)) return it
    }
    return null
}