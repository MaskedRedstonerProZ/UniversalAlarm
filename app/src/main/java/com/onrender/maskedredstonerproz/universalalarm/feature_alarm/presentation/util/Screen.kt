package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util

sealed class Screen(
	private val route: String,
	val name: String
) {

	/**
	 * Main Screen info object
	 * @author MaskedRedstonerProZ
	 */
	data object MainScreen: Screen("main_screen", "Home")

	/**
	 * About Screen info object
	 * @author MaskedRedstonerProZ
	 */
	data object AboutScreen: Screen("about_screen", "About")

	/**
	 * Invoke method that return's the screen's route when the [Screen] is invoked like a method
	 * @return The screen's route
	 * @author MaskedRedstonerProZ
	 */
	open operator fun invoke() = this.route

}