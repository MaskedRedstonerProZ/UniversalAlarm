package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.AlarmSchedulerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.runBasedOnDueDateOf
import java.time.LocalDateTime

/**
 * The use case for cancelling [Alarm]s
 * @param scheduler The scheduler for the [Alarm]s
 * @author MaskedRedstonerProZ
 */
class CancelAlarm(
    private val scheduler: AlarmSchedulerApi
) {
    operator fun invoke(alarm: Alarm) {
        runBasedOnDueDateOf(alarm) {
            scheduler.cancel(alarm.copy(
                dueTime = LocalDateTime.of(it, alarm.dueTime.toLocalTime()))
            )
        }
    }
}
