package com.onrender.maskedredstonerproz.universalalarm.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.runtime.Composable

private val DarkColorPallete = darkColors(
    primary = red,
    background = grey,
    surface = black,
    onSurface = white,
    onPrimary = blue
)

/**
 * The theme of the project
 * @author MaskedRedstonerProZ
 */
@Composable
fun UniversalAlarmTheme(
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colors = DarkColorPallete,
        shapes = Shapes,
        typography = Typography,
        content = content
    )
}