package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.repository

import com.onrender.maskedredstonerproz.universalalarm.AlarmDatabase
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * The repository for accessing the [AlarmDatabase]
 * @author MaskedRedstonerProZ
 */
interface AlarmRepository {

    /**
     * Gets the [Alarm] database entry by the given [id]
     * @param id The [id] of the [Alarm]
     * @return The [Alarm] with the given [id], null if no such [Alarm] is found
     * @author MaskedRedstonerProZ
     */
    suspend fun getAlarmById(id: Long): Alarm?

    /**
     * Gets all the [Alarm] database entries
     * @return A [Flow] containing a [List] of the [Alarm]s
     * @author MaskedRedstonerProZ
     */
    fun getAllAlarms(): Flow<List<Alarm>>

    /**
     * Deletes the [Alarm] database entry by the given [id]
     * @param id The [id] of the [Alarm]
     * @author MaskedRedstonerProZ
     */
    suspend fun deleteAlarmById(id: Long)

    /**
     * Inserts the [Alarm] with the given attributes
     * @param label The label of the [Alarm], (eg, School, Work, Meeting etc.)
     * @param dueTime The time for which the [Alarm] is scheduled
     * @param activePeriodAndWeekDayMap The [Map] containing a [Pair]which represents the active period
     * start and end date(s), and the weekdays for which the alarm should be scheduled
     * @author MaskedRedstonerProZ
     */
    suspend fun insertAlarm(label: String, dueTime: LocalDateTime, activePeriodAndWeekDayMap: Map<Pair<LocalDate, LocalDate>, Array<String>>)

}