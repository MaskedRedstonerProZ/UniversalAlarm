package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import android.app.AlarmManager

/**
 * The scheduler api for interacting with [AlarmManager]
 * @author MaskedRedstonerProZ
 */
interface AlarmSchedulerApi {

    /**
     * Schedules the [Alarm]
     * @param alarm The [Alarm] to schedule
     * @author MaskedRedstonerProZ
     */
    fun schedule(alarm: Alarm)

    /**
     * Cancels the [Alarm]
     * @param alarm The [Alarm] to cancel
     * @author MaskedRedstonerProZ
     */
    fun cancel(alarm: Alarm)

}