package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.screens.about_screen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.onrender.maskedredstonerproz.universalalarm.R
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Constants.EMAIL_TAG
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme

/**
 * Screen that contains info about me, the app, and the purpose of it's creation
 * @author MaskedRedstonerProZ
 */
@Composable
fun AboutScreen() {
	Column(
		verticalArrangement = Arrangement.Center,
		horizontalAlignment = Alignment.CenterHorizontally,
		modifier = Modifier
			.background(MaterialTheme.colors.background)
			.padding(10.dp)
			.fillMaxSize()
	) {

		val annotatedLinkString: AnnotatedString = buildAnnotatedString {

			val appInfo = stringResource(id = R.string.app_info)
			val email = stringResource(id = R.string.email)
			val copyright = stringResource(id = R.string.copyright)

			append("$appInfo ")
			withStyle(
				SpanStyle(
					color = MaterialTheme.colors.primary,
					fontSize = 20.sp,
					textDecoration = TextDecoration.Underline
				)
			) {
				append(email)

				addStringAnnotation(
					tag = EMAIL_TAG,
					annotation = "mailto:$email",
					start = appInfo.lastIndex,
					end = appInfo.lastIndex + email.lastIndex
				)
			}
			append(copyright)
		}


		val uriHandler = LocalUriHandler.current
		Column(
			modifier = Modifier.verticalScroll(rememberScrollState())
		) {
			ClickableText(
				text = annotatedLinkString,
				style = MaterialTheme.typography.subtitle2.copy(
					fontSize = 17.sp
				),
				onClick = {
					annotatedLinkString
						.getStringAnnotations(EMAIL_TAG, it, it)
						.firstOrNull()?.let { stringAnnotation ->
							uriHandler.openUri(stringAnnotation.item)
						}
				}
			)
		}
	}
}

@Preview
@Composable
private fun AboutScreenPreview() {
	UniversalAlarmTheme {
		AboutScreen()
	}
}