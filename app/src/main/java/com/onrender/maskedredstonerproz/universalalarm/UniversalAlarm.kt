package com.onrender.maskedredstonerproz.universalalarm

import android.app.Application
import com.onrender.maskedredstonerproz.universalalarm.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * The application class
 * @author MaskedRedstonerProZ
 */
class UniversalAlarm: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {

            androidLogger(Level.ERROR)

            androidContext(this@UniversalAlarm)

            modules(
                listOf(
                    appModule
                )
            )

        }
    }
}