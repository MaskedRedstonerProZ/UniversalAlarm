package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.serializers

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.SerialKind
import kotlinx.serialization.descriptors.buildSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

/**
 * Custom serializer for String array
 * @author MaskedRedstonerProZ
 */
object StringArraySerializer : KSerializer<Array<String>> {
	@OptIn(InternalSerializationApi::class, ExperimentalSerializationApi::class)
	override val descriptor: SerialDescriptor = buildSerialDescriptor("StringArray", SerialKind.CONTEXTUAL)

	override fun serialize(encoder: Encoder, value: Array<String>) {
		encoder.encodeSerializableValue(ListSerializer(String.serializer()), value.toList())
	}

	override fun deserialize(decoder: Decoder): Array<String> {
		return decoder.decodeSerializableValue(ListSerializer(String.serializer())).toTypedArray()
	}
}