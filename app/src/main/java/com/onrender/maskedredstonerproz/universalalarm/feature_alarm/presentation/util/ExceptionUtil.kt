package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util

fun printException(e: Throwable) {
    println("Exception message: ${e.message}")
    println("Exception cause: ${e.cause}")
    println("Exception stack trace: ${e.stackTraceToString()}")
    if(e.suppressed.isNotEmpty()) {
        println("Suppressed exceptions: ")
        e.suppressed.forEach(::printException)
    }
}