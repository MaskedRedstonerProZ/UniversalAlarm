package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.repository.AlarmRepository
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import kotlinx.coroutines.flow.Flow

/**
 * The use case for getting all the [Alarm]s
 * @param repository The [AlarmRepository] for accessing the database
 * @author MaskedRedstonerProZ
 */
class GetAllAlarms(
    private val repository: AlarmRepository
) {

    // invocation operator
    operator fun invoke(): Flow<List<Alarm>> = repository.getAllAlarms()

}