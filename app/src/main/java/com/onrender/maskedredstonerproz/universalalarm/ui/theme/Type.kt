package com.onrender.maskedredstonerproz.universalalarm.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.onrender.maskedredstonerproz.universalalarm.R

/**
 * The font for the project
 * @author MaskedRedstonerProZ
 */
val Orbitron = FontFamily(
    Font(
        R.font.orbitron
    )
)

/**
 * The project's typography
 *
 * h1 - due time selector in alarm dialog
 *
 * h2 - due time display text in alarm list
 *
 * h3 - am/pm selector in alarm dialog
 *
 * subtitle1 - alarm label text box placeholder
 *
 * subtitle2 - app bar text, about screen text, alarm active period selector in alarm dialog
 *
 * caption - alarm label in alarm list, weekly repetition settings display text in alarm list, am/pm label in alarm list, dialog title
 * @author MaskedRedstonerProZ
 */

val Typography = Typography(
    defaultFontFamily = Orbitron,
    h1 = TextStyle(
        fontWeight = FontWeight.Bold,
        fontSize = 64.sp
    ),
    h2 = TextStyle(
        fontWeight = FontWeight.Bold,
        fontSize = 43.sp
    ),
    h3 = TextStyle(
        fontWeight = FontWeight.Bold,
        fontSize = 37.sp
    ),
    subtitle1 = TextStyle(
        fontWeight = FontWeight.Bold,
        fontSize = 32.sp
    ),
    subtitle2 = TextStyle(
        fontWeight = FontWeight.Bold,
        fontSize = 27.sp
    ),
    button = TextStyle(
        fontWeight = FontWeight.Bold,
        fontSize = 24.sp
    ),
    caption = TextStyle(
        fontWeight = FontWeight.Bold,
        fontSize = 16.sp
    )
)