package com.onrender.maskedredstonerproz.universalalarm.di

import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.android.AndroidSqliteDriver
import com.onrender.maskedredstonerproz.universalalarm.AlarmDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/**
 * Test application module for dependency injection
 * @author MaskedRedstonerProZ
 */
val testAppModule = module {

    single {
        val driver: SqlDriver = AndroidSqliteDriver(AlarmDatabase.Schema, androidContext(), null)
        AlarmDatabase(driver)
    }
}