package com.onrender.maskedredstonerproz.universalalarm.util

/**
 * Test tag definition object
 * @author MaskedRedstonerProZ
 */
object TestTag {

    /**
     * Test tag for the alarm's weekly repetition settings selector
     * @author MaskedRedstonerProZ
     */
    const val ALARM_WEEKDAY_REPETITION_SELECTOR_TEST_TAG = "alarm_weekday_repetition_selector"

    /**
     * Test tag for the standard text field
     * @author MaskedRedstonerProZ
     */
    const val STANDARD_TEXT_FIELD_TEST_TAG = "standard_text_field"

    /**
     * Test tag for the alarm dialog
     * @author MaskedRedstonerProZ
     */
    const val ALARM_DIALOG_TEST_TAG = "alarm_dialog"

    /**
     * Test tag for the alarm's active periods selector
     * @author MaskedRedstonerProZ
     */
    const val ALARM_ACTIVE_PERIODS_SELECTOR_TEST_TAG = "alarm_active_periods_selector_test_tag"

}