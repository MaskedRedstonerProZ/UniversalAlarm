package com.onrender.maskedredstonerproz.universalalarm

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner

/**
 * Custom test runner class
 * @author MaskedRedstonerProZ
 */
@Suppress("unused")
class TestRunner: AndroidJUnitRunner() {
    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application {
        return super.newApplication(cl, TestUniversalAlarm::class.java.name, context)
    }
}