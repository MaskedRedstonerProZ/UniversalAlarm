package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.dialogs

import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme
import com.onrender.maskedredstonerproz.universalalarm.util.TestTag
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.time.LocalDate

class AddNewAlarmDialogTest {

	@get:Rule
	val composeTestRule = createComposeRule()

	private var isAlarmLabelAdded = false

	private var isNewActivePeriodAdded = false

	private var isDueTimeSelectionDialogShown = false

	private var isActivePeriodSelectionDialogShown = false

	private var areWeekdaysSelected = false

	private var isAlarmDialogDismissed = false

	private var shouldAlarmBeCreated = false

	@Before
	fun setUp() {

		isAlarmLabelAdded = mutableStateOf(false).value

		isNewActivePeriodAdded = mutableStateOf(false).value

		isDueTimeSelectionDialogShown = mutableStateOf(false).value

		isActivePeriodSelectionDialogShown = mutableStateOf(false).value

		areWeekdaysSelected = mutableStateOf(false).value

		isAlarmDialogDismissed = mutableStateOf(false).value

		shouldAlarmBeCreated = mutableStateOf(false).value

		composeTestRule.setContent {
			UniversalAlarmTheme {
				AddNewAlarmDialog(
					modifier = Modifier.testTag(TestTag.ALARM_DIALOG_TEST_TAG),
					alarmLabel = "",
					alarmDueTimeString = "00: 00 am",
					activePeriods = listOf(LocalDate.now() to LocalDate.now().plusDays(7)),
					onAlarmLabelChanged = {
						isAlarmLabelAdded = it.isNotEmpty()
					},
					onAlarmDueTimeSelectionDialogShown = {
						isDueTimeSelectionDialogShown = true
					},
					onActivePeriodSelectionDialogShown = {
						isActivePeriodSelectionDialogShown = true
					},
					onActivePeriodAdded = {
						isNewActivePeriodAdded = true
					},
					onWeekdayAdded = {
						areWeekdaysSelected = true
					},
					onWeekdayRemoved = {
						areWeekdaysSelected = false
					},
					onDismiss = {
						isAlarmDialogDismissed = true
					},
					onAlarmCreated = {
						shouldAlarmBeCreated = true
					}
				)
			}
		}
	}

	@Test
	fun typeAlarmLabel_AlarmLabelAdded() {
		composeTestRule.apply {

			onNodeWithTag(TestTag.ALARM_DIALOG_TEST_TAG).onChildAt(1).performTextInput("A")

			assertThat(isAlarmLabelAdded).isTrue()
		}
	}

	@Test
	fun clickOnDueTimeDisplay_DueTimeSelectionDialogShown() {
		composeTestRule.apply {

			onNodeWithTag(TestTag.ALARM_DIALOG_TEST_TAG).onChildAt(2).performClick()

			assertThat(isDueTimeSelectionDialogShown).isTrue()

		}
	}

	@Test
	fun clickOnActivePeriodListItem_ActivePeriodSelectionDialogShown() {
		composeTestRule.apply {

			onNodeWithTag(TestTag.ALARM_DIALOG_TEST_TAG).onChildAt(10).onChildAt(0).performClick()

			assertThat(isActivePeriodSelectionDialogShown).isTrue()

		}
	}

	@Test
	fun clickOnAddNewActivePeriod_NewActivePeriodAdded() {
		composeTestRule.apply {

			onNodeWithTag(TestTag.ALARM_DIALOG_TEST_TAG).onChildAt(10).onChildAt(3).performClick()

			assertThat(isNewActivePeriodAdded).isTrue()

		}
	}

	@Test
	fun clickOnWeekdaySelector_WeekdaySelected() {
		composeTestRule.apply {

			onNodeWithTag(TestTag.ALARM_DIALOG_TEST_TAG).onChildAt(3).performClick()

			assertThat(areWeekdaysSelected).isTrue()

		}
	}

	@Test
	fun clickOnCreateAlarm_AlarmCreated() {
		composeTestRule.apply {

			onNodeWithTag(TestTag.ALARM_DIALOG_TEST_TAG).onChildAt(2)
				.performClick() // due time selected

			onNodeWithTag(TestTag.ALARM_DIALOG_TEST_TAG).onChildAt(10).onChildAt(0)
				.performClick() // active period specified

			onNodeWithTag(TestTag.ALARM_DIALOG_TEST_TAG).onChildAt(10).onChildAt(3)
				.performClick() // new active period added

			onNodeWithTag(TestTag.ALARM_DIALOG_TEST_TAG).onChildAt(3)
				.performClick() // weekday selected

			onNodeWithTag(TestTag.ALARM_DIALOG_TEST_TAG).onChildAt(11).performClick()

			assertThat(
				isDueTimeSelectionDialogShown
						&& isActivePeriodSelectionDialogShown
						&& isNewActivePeriodAdded
						&& areWeekdaysSelected
						&& shouldAlarmBeCreated
			).isTrue()

		}
	}
}