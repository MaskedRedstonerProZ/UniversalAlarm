package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onAllNodesWithTag
import androidx.compose.ui.test.performClick
import androidx.test.filters.MediumTest
import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.util.TestTag
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme

import org.junit.Before
import org.junit.Rule
import org.junit.Test

@MediumTest
class AlarmWeekdayRepetitionSelectorTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    private lateinit var selected: SnapshotStateList<String>

    private lateinit var weekdays: List<String>

    @Before
    fun setUp() {
        selected = mutableStateListOf()

        weekdays = "M T W TH F S SU".split(" ")

        composeTestRule.setContent {
            UniversalAlarmTheme {
                AlarmWeekdayRepetitionSelector(
                    selected = selected,
                    modifier = Modifier.testTag(TestTag.ALARM_WEEKDAY_REPETITION_SELECTOR_TEST_TAG),
                    onWeekdayAddedToSelection = selected::add,
                    onWeekdayRemovedFromSelection = selected::remove
                )
            }
        }
    }

    @Test
    fun clickAlarmSelectorItem_RespectiveItemGetsSelected() {
        composeTestRule.apply {

            onAllNodesWithTag(TestTag.ALARM_WEEKDAY_REPETITION_SELECTOR_TEST_TAG)[0].performClick()

        }

        assertThat(selected).contains(weekdays[0])
    }
}