package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.test.assertContentDescriptionEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.test.filters.MediumTest
import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme
import com.onrender.maskedredstonerproz.universalalarm.util.TestTag
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.time.LocalDate

@MediumTest
class AlarmActivePeriodsSelectorTest {

	@get:Rule
	val composeTestRule = createComposeRule()

	private lateinit var activePeriods: SnapshotStateList<Pair<LocalDate, LocalDate>>

	private var shouldActivePeriodSelectionDialogBeShown = false

	private var isBeginningOfActivePeriodSelected = false

	private var shouldWeekdaySelectionBeReset = false

	private val defaultDatePair = LocalDate.now() to LocalDate.now().plusDays(7)

	@Before
	fun setUp() {

		activePeriods = mutableStateListOf(defaultDatePair)

		shouldActivePeriodSelectionDialogBeShown = mutableStateOf(false).value

		isBeginningOfActivePeriodSelected = mutableStateOf(false).value

		shouldWeekdaySelectionBeReset = mutableStateOf(false).value

		composeTestRule.setContent {
			UniversalAlarmTheme {
				AlarmActivePeriodSelector(
					modifier = Modifier.testTag(TestTag.ALARM_ACTIVE_PERIODS_SELECTOR_TEST_TAG),
					activePeriods = activePeriods,
					onActivePeriodAdded = {
						activePeriods.add(defaultDatePair)
					},
					onActivePeriodSelectionDialogShown = {
						shouldActivePeriodSelectionDialogBeShown = true
						isBeginningOfActivePeriodSelected = it
					},
					resetWeekdaySelection = {
						shouldWeekdaySelectionBeReset = true
					}
				)
			}
		}
	}

	@Test
	fun clickedShowActivePeriodSelectionDialogForBeginningDate_ActivePeriodSelectionDialogForBeginningDateShown() {
		composeTestRule.apply {

			onNodeWithTag(TestTag.ALARM_ACTIVE_PERIODS_SELECTOR_TEST_TAG).onChildAt(0).performClick()

			assertThat(shouldActivePeriodSelectionDialogBeShown && isBeginningOfActivePeriodSelected).isTrue()
		}
	}

	@Test
	fun clickedShowActivePeriodSelectionDialogForEndingDate_ActivePeriodSelectionDialogForEndingDateShown() {
		composeTestRule.apply {

			onNodeWithTag(TestTag.ALARM_ACTIVE_PERIODS_SELECTOR_TEST_TAG).onChildAt(2).performClick()

			assertThat(shouldActivePeriodSelectionDialogBeShown && !isBeginningOfActivePeriodSelected).isTrue()
		}
	}

	@Test
	fun clickedAddActivePeriod_NewActivePeriodAdded() {
		composeTestRule.apply {

			onNodeWithTag(TestTag.ALARM_ACTIVE_PERIODS_SELECTOR_TEST_TAG).onChildAt(3).performClick()

			assertThat(activePeriods.size).isAtLeast(2)
		}
	}

	@Test
	fun clickedAddActivePeriod_AddButtonOnlyShowsUpOnLastPeriodInList() {
		composeTestRule.apply {

			onNodeWithTag(TestTag.ALARM_ACTIVE_PERIODS_SELECTOR_TEST_TAG).onChildAt(3).performClick()

			onNodeWithTag(TestTag.ALARM_ACTIVE_PERIODS_SELECTOR_TEST_TAG).onChildAt(6).assertContentDescriptionEquals("Add new active period")
		}
	}

}