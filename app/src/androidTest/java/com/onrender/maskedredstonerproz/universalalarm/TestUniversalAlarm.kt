package com.onrender.maskedredstonerproz.universalalarm

import android.app.Application
import com.onrender.maskedredstonerproz.universalalarm.di.testAppModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * The test application class
 * @author MaskedRedstonerProZ
 */
class TestUniversalAlarm: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {

            androidLogger(Level.ERROR)

            androidContext(this@TestUniversalAlarm)

            modules(
                listOf(
                    testAppModule
                )
            )

        }
    }
}