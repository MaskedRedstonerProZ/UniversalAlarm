package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.components

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performTextInput
import androidx.test.filters.MediumTest
import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.util.TestTag
import com.onrender.maskedredstonerproz.universalalarm.ui.theme.UniversalAlarmTheme

import org.junit.Before
import org.junit.Rule
import org.junit.Test

@MediumTest
class StandardTextFieldTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    private lateinit var testValue: MutableState<String>

    @Before
    fun setUp() {

        testValue = mutableStateOf("")

        composeTestRule.setContent {

            UniversalAlarmTheme {
                StandardTextField(
                    value = testValue.value,
                    modifier = Modifier.testTag(TestTag.STANDARD_TEXT_FIELD_TEST_TAG),
                    placeholder = {
                        Text(
                            text = "Placeholder",
                            style = MaterialTheme.typography.subtitle1
                        )
                    },
                    onValueChanged = {
                        testValue.value = it
                    }
                )
            }
        }
    }

    @Test
    fun typeValueInStandardTextField_Success() {

        val text = "asdfghjkl"

        composeTestRule.apply {

            onNodeWithTag(TestTag.STANDARD_TEXT_FIELD_TEST_TAG).performTextInput(text)

        }

        assertThat(testValue.value).isEqualTo(text)
    }
}