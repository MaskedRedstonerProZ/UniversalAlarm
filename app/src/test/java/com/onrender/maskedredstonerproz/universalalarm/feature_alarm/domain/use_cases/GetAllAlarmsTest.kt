package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.repository.AlarmRepository
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.repository.FakeAlarmRepositoryImpl
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * The test class for the [GetAllAlarms] use case
 * @author MaskedRedstonerProZ
 */
class GetAllAlarmsTest {

    private lateinit var getAllAlarms: GetAllAlarms

    /**
     * The test implementation of the [AlarmRepository]
     * @author MaskedRedstonerProZ
     */
    private lateinit var fakeRepository: FakeAlarmRepositoryImpl

    /**
     * The setup function that runs before every test case
     * @author MaskedRedstonerProZ
     */
    @Before
    fun setUp() {
        fakeRepository = FakeAlarmRepositoryImpl()
        getAllAlarms = GetAllAlarms(fakeRepository)

        runTest {
            ('a'..'z').forEach {
                fakeRepository.insertAlarm(it.toString(), LocalDateTime.now(), mapOf(Pair(Pair(
                    LocalDate.now(), LocalDate.now()), arrayOf(it.toString()))))
            }
        }
    }

    /**
     * Test case to test if getting all alarms results in success
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `get all alarms from database, success`() {
        runTest {
            val alarms = getAllAlarms()

            alarms.collectLatest {
                assertThat(it.count()).isEqualTo(('a'..'z').count())
            }
        }
    }
}