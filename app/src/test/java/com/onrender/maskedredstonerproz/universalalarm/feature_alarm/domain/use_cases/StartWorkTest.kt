package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import androidx.work.PeriodicWorkRequestBuilder
import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api.FakeWorkManagerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.WorkManagerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.framework.workers.AlarmActivePeriodLoopWorker
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

/**
 * The test class for the [StartWork] use case
 * @author MaskedRedstonerProZ
 */
class StartWorkTest {

    /**
     * The use case instance for starting a worker
     * @author MaskedRedstonerProZ
     */
    private lateinit var startWork: StartWork

    /**
     * The test implementation of the [WorkManagerApi]
     * @author MaskedRedstonerProZ
     */
    private lateinit var workManager: FakeWorkManagerApi

    /**
     * The setup function that runs before every test case
     * @author MaskedRedstonerProZ
     */
    @Before
    fun setUp() {
        workManager = FakeWorkManagerApi()
        startWork = StartWork(workManager)
    }

    /**
     * Test case that tests if attempting to start a periodic work results in it being actually started
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `start periodic work with the AlarmActivePeriodLoopWorker, started`() {

        val workRequest =
            PeriodicWorkRequestBuilder<AlarmActivePeriodLoopWorker>(1, TimeUnit.DAYS)
                .build()

        startWork(workRequest, "Test work")

        assertThat(workManager.isPeriodicWorkStarted).isTrue()
    }
}