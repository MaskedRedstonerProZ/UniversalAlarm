package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.repository

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.repository.AlarmRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * Fake test implementation of [AlarmRepository]
 * @author MaskedRedstonerProZ
 */
class FakeAlarmRepositoryImpl: AlarmRepository {

    /**
     * The list of [Alarm]s that allows us to fake the behaviour of the real repository
     * @author MaskedRedstonerProZ
     */
    val alarms = mutableListOf<Alarm>()

    override suspend fun getAlarmById(id: Long): Alarm? {
        return alarms.find { it.id == id }
    }

    override fun getAllAlarms(): Flow<List<Alarm>> {
        return flowOf(alarms)
    }

    override suspend fun deleteAlarmById(id: Long) {
        alarms.remove(alarms.find { it.id == id })
    }

    override suspend fun insertAlarm(
        label: String,
        dueTime: LocalDateTime,
        activePeriodAndWeekDayMap: Map<Pair<LocalDate, LocalDate>, Array<String>>
    ) {
        alarms.add(
            Alarm(
                id = alarms.size.toLong(),
                label = label,
                dueTime = dueTime,
                activePeriodsAndWeekdays = activePeriodAndWeekDayMap
            )
        )
    }
}