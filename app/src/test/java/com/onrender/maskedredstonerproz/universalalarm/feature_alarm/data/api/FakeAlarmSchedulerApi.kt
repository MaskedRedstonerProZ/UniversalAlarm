package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api

import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.AlarmSchedulerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import java.time.LocalDateTime

/**
 * The fake [AlarmSchedulerApi] implementation for testing, which mimics the real one's behaviour
 * @author MaskedRedstonerProZ
 */
class FakeAlarmSchedulerApi: AlarmSchedulerApi {

    /**
     * The amount of days for which the [Alarm] is scheduled
     * @author MaskedRedstonerProZ
     */
    var scheduleCounter = 0
        private set

    /**
     * The dates for which the alarm is scheduled
     * @author MaskedRedstonerProZ
     */
    private val scheduledDates: ArrayList<LocalDateTime> = arrayListOf()

    override fun schedule(alarm: Alarm) {
        scheduleCounter++
        scheduledDates.add(alarm.dueTime)
    }

    override fun cancel(alarm: Alarm) {
        scheduleCounter--
        scheduledDates.remove(alarm.dueTime)
    }

    /**
     * A public getter for the scheduled dates which makes external manipulation of the list impossible
     * @author MaskedRedstonerProZ
     */
    fun getScheduledDates() = scheduledDates.toList()

}