package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api.FakeAlarmSchedulerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.AlarmSchedulerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.Constants
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.ext.countWeekday
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.ext.getNextWeekdayDate
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * The test class for the [ScheduleAlarm] use case
 * @author MaskedRedstonerProZ
 */
class ScheduleAlarmTest {

    /**
     * The use case instance for scheduling an [Alarm]
     * @author MaskedRedstonerProZ
     */
    private lateinit var scheduleAlarm: ScheduleAlarm

    /**
     * The test implementation instance of the [AlarmSchedulerApi]
     * @author MaskedRedstonerProZ
     */
    private lateinit var scheduler: FakeAlarmSchedulerApi

    /**
     * The setup function that runs before every test case
     * @author MaskedRedstonerProZ
     */
    @Before
    fun setUp() {
        scheduler = FakeAlarmSchedulerApi()
        scheduleAlarm = ScheduleAlarm(scheduler)
    }

    /**
     * Test case to test if scheduling an alarm results in it being scheduled for every required day within the active period
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `schedule alarm, scheduled for every chosen weekday in active period`() {

        val weekdayRepetitionSettings = arrayOf(
            "M",
            "T",
            "W",
            "TH",
            "F",
            "S",
            "SU"
        )

        val alarmToSchedule = Alarm(
            id = 1,
            label = "Test alarm",
            dueTime = LocalDateTime.now(),
            activePeriodsAndWeekdays = mapOf(
                LocalDate.now() to LocalDate.now().plusDays(7) to weekdayRepetitionSettings
            )
        )

        scheduleAlarm(alarmToSchedule)

        var totalCount = 0

        weekdayRepetitionSettings.forEach {
            totalCount += it.countWeekday(LocalDate.now(), LocalDate.now().plusDays(7))
        }

        assertThat(scheduler.scheduleCounter).isEqualTo(totalCount)
    }

    @Test
    fun `schedule alarm, dueTime date corresponds to date it is supposed to be scheduled on`() {

        val weekdays = Constants.WEEKDAYS.split(',').map {
            it.trim().uppercase()
        }

        val weekdayRepetitionSettings = arrayOf(
            "M",
            "W",
            "F",
        )

        val alarmToSchedule = Alarm(
            id = 1,
            label = "Test alarm",
            dueTime = LocalDateTime.now(),
            activePeriodsAndWeekdays = mapOf(
                LocalDate.now() to LocalDate.now().plusDays(7) to weekdayRepetitionSettings
            )
        )

        scheduleAlarm(alarmToSchedule)

        println(scheduler.getScheduledDates()[0].toString())

        scheduler.getScheduledDates().forEachIndexed { i, it ->
            assertThat(it.toLocalDate()).isEqualTo(weekdays.find { it.startsWith(weekdayRepetitionSettings[i]) }!!.getNextWeekdayDate())
        }
    }

}
