package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.ext

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.temporal.TemporalAdjusters
import java.util.Locale

/**
 * Counts the occurrences of the given weekday in a period
 * @return The amount of the weekdays
 * @param startDate The start of the period.
 * @param endDate The end of the period.
 * @author MaskedRedstonerProZ
 */
fun String.countWeekday(startDate: LocalDate, endDate: LocalDate): Int {
    var count = 0
    onUpcomingWeekdayOccurrences(startDate, endDate) {
        count++
    }
    return count
}

/**
 * Gets the date of the next occurrence of the given weekday
 * @author MaskedRedstonerProZ
 */
fun String.getNextWeekdayDate(): LocalDate? {
    // Convert the uppercase weekday string to a DayOfWeek enum value
    val targetWeekday = DayOfWeek.entries.find {
        it.name == this.uppercase(Locale.getDefault())
    }

    // Check if the targetWeekday is found
    if (targetWeekday != null) {
        // Get the current date
        val currentDate = LocalDate.now()

        // Adjust the current date to the nearest future occurrence of the given weekday
        return currentDate.with(TemporalAdjusters.nextOrSame(targetWeekday))
    }

    // Return null if the weekday string is invalid
    return null
}