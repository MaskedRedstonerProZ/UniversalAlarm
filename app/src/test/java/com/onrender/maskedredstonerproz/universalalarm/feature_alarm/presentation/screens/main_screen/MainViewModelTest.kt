package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.screens.main_screen

import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api.FakeAlarmSchedulerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api.FakeWorkManagerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.repository.FakeAlarmRepositoryImpl
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.AlarmSchedulerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.WorkManagerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions.AlarmActivePeriodStartAfterEndException
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions.AlarmLabelLengthOverflowException
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions.AlarmWeeklyRepetitionSettingsBlankException
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.repository.AlarmRepository
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.AddAlarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.AlarmUseCases
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.CancelAlarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.CancelWork
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.DeleteAlarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.GetAlarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.GetAllAlarms
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.ScheduleAlarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases.StartWork
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.Constants
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Event
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Event.AddAlarmToDatabase
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Event.DeleteAlarmFromDatabase
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Event.FetchAlarmsFromDatabase
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Event.ShowCreateNewAlarmDialog
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Event.StartRescheduleWork
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Event.StopRescheduleWork
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Event.UpdateActivePeriodsAndWeekdaysMap
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.presentation.util.Event.UpdateAlarmLabel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * Test class for the [MainViewModel]
 * @author MaskedRedstonerProZ
 */
@OptIn(ExperimentalCoroutinesApi::class)
class MainViewModelTest {

    /**
     * The test implementation of the [AlarmRepository]
     * @author MaskedRedstonerProZ
     */
    private lateinit var repository: FakeAlarmRepositoryImpl

    /**
     * The test implementation instance of the [AlarmSchedulerApi]
     * @author MaskedRedstonerProZ
     */
    private lateinit var scheduler: FakeAlarmSchedulerApi

    /**
     * The test implementation of the [WorkManagerApi]
     * @author MaskedRedstonerProZ
     */
    private lateinit var workManager: FakeWorkManagerApi

    /**
     * The use case collection instance
     * @author MaskedRedstonerProZ
     */
    private lateinit var alarmUseCases: AlarmUseCases

    /**
     * The main View Model
     * @author MaskedRedstonerProZ
     */
    private lateinit var mainViewModel: MainViewModel

    /**
     * The setup function that runs before every test case
     * @author MaskedRedstonerProZ
     */
    @Before
    fun setUp() {
        repository = FakeAlarmRepositoryImpl()
        scheduler = FakeAlarmSchedulerApi()
        workManager = FakeWorkManagerApi()
        alarmUseCases = AlarmUseCases(
            getAlarm = GetAlarm(repository),
            getAllAlarms = GetAllAlarms(repository),
            deleteAlarm = DeleteAlarm(repository),
            addAlarm = AddAlarm(repository),
            scheduleAlarm = ScheduleAlarm(scheduler),
            cancelAlarm = CancelAlarm(scheduler),
            startWork = StartWork(workManager),
            cancelWork = CancelWork(workManager)
        )
        mainViewModel = MainViewModel(alarmUseCases, UnconfinedTestDispatcher())
    }

    /**
     * Test case that checks if the [FetchAlarmsFromDatabase] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event FetchAlarmsFromDatabase, data fetched successfully`() {
        runTest {

            repository.insertAlarm(
                "Test",
                LocalDateTime.now(),
                mapOf(LocalDate.now() to LocalDate.now() to arrayOf("M", "W"))
            )

            mainViewModel.onEvent(FetchAlarmsFromDatabase)

            assertThat(mainViewModel.alarmsListState).isNotEmpty()
        }
    }

    /**
     * Test case that checks if the [ShowCreateNewAlarmDialog] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event ShowCreateNewAlarmDialog, creation dialog gets shown`() {
        mainViewModel.onEvent(ShowCreateNewAlarmDialog)

        assertThat(mainViewModel.shouldShowNewAlarmDialogState).isTrue()
    }

    /**
     * Test case that checks if the [UpdateAlarmLabel] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event UpdateAlarmLabel, alarm label text gets updated`() {
        val testLabel = "test"

        mainViewModel.onEvent(UpdateAlarmLabel(testLabel))

        assertThat(mainViewModel.alarmLabelState).isEqualTo(testLabel)
    }

    /**
     * Test case that checks if the [AddAlarmToDatabase] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event AddAlarmToDatabase, alarm added`() {
        val testMapKey = LocalDate.now() to LocalDate.now()

        val testActivePeriodsAndWeekdaysMap = mapOf(testMapKey to arrayOf("M", "W"))

        mainViewModel.onEvent(UpdateActivePeriodsAndWeekdaysMap(testActivePeriodsAndWeekdaysMap))

        mainViewModel.onEvent(AddAlarmToDatabase)

        assertThat(repository.alarms.count()).isEqualTo(1)
    }

    /**
     * Test case that checks if the [AddAlarmToDatabase] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event AddAlarmToDatabase, alarm added to list state`() {

        val testAlarmLabel = "Test Alarm"

        mainViewModel.onEvent(UpdateAlarmLabel(testAlarmLabel))

        val testMapKey = LocalDate.now() to LocalDate.now()

        val testActivePeriodsAndWeekdaysMap = mapOf(testMapKey to arrayOf("M", "W"))

        mainViewModel.onEvent(UpdateActivePeriodsAndWeekdaysMap(testActivePeriodsAndWeekdaysMap))

        mainViewModel.onEvent(AddAlarmToDatabase)

        assertThat(mainViewModel.alarmsListState.find { it.label == testAlarmLabel }).isNotEqualTo(
            null
        )
    }

    /**
     * Test case that checks if the [AddAlarmToDatabase] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event AddAlarmToDatabase with invalid label, error gets logged properly`() {
        runTest {
            var testAlarmLabel = ""

            (0..Constants.MAX_ALARM_LABEL_LENGTH).forEach {
                testAlarmLabel += it.toString()[0]
            }

            mainViewModel.onEvent(UpdateAlarmLabel(testAlarmLabel))

            val testMapKey = LocalDate.now() to LocalDate.now()

            val testActivePeriodsAndWeekdaysMap = mapOf(testMapKey to arrayOf("M", "W"))

            mainViewModel.onEvent(UpdateActivePeriodsAndWeekdaysMap(testActivePeriodsAndWeekdaysMap))

            mainViewModel.onEvent(AddAlarmToDatabase)

            backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
                mainViewModel.errorFlow.collectLatest {
                    assertThat(it).isInstanceOf(AlarmLabelLengthOverflowException::class.java)
                }
            }
        }
    }

    /**
     * Test case that checks if the [AddAlarmToDatabase] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event AddAlarmToDatabase with an active period that happens to start before it ends, error gets logged properly`() {
        runTest {
            val testMapKey = LocalDate.now().minusDays(1) to LocalDate.now()

            val testActivePeriodsAndWeekdaysMap = mapOf(testMapKey to arrayOf("M", "W"))

            mainViewModel.onEvent(UpdateActivePeriodsAndWeekdaysMap(testActivePeriodsAndWeekdaysMap))

            mainViewModel.onEvent(AddAlarmToDatabase)

            backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
                mainViewModel.errorFlow.collectLatest {
                    assertThat(it).isInstanceOf(AlarmActivePeriodStartAfterEndException::class.java)
                }
            }
        }
    }

    /**
     * Test case that checks if the [AddAlarmToDatabase] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event AddAlarmToDatabase with an empty activePeriodsAndWeekdays map, error gets logged properly`() {
        runTest {
            val testMapKey = LocalDate.now() to LocalDate.now()

            val testActivePeriodsAndWeekdaysMap = mapOf(testMapKey to arrayOf("M", "W"))

            mainViewModel.onEvent(UpdateActivePeriodsAndWeekdaysMap(testActivePeriodsAndWeekdaysMap))

            mainViewModel.onEvent(AddAlarmToDatabase)

            backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
                mainViewModel.errorFlow.collectLatest {
                    assertThat(it).isInstanceOf(AlarmWeeklyRepetitionSettingsBlankException::class.java)
                }
            }
        }
    }

    /**
     * Test case that checks if the [AddAlarmToDatabase] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event AddAlarmToDatabase with an empty weekly repetition settings array, error gets logged properly`() {
        runTest {
            val testMapKey = LocalDate.now() to LocalDate.now()

            val testActivePeriodsAndWeekdaysMap = mapOf(testMapKey to emptyArray<String>())

            mainViewModel.onEvent(UpdateActivePeriodsAndWeekdaysMap(testActivePeriodsAndWeekdaysMap))

            mainViewModel.onEvent(AddAlarmToDatabase)

            backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
                mainViewModel.errorFlow.collectLatest {
                    assertThat(it).isInstanceOf(AlarmWeeklyRepetitionSettingsBlankException::class.java)
                }
            }
        }
    }

    /**
     * Test case that checks if the [AddAlarmToDatabase] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event AddAlarmToDatabase with a weekly repetition settings array with an empty string, error gets logged properly`() {
        runTest {
            val testMapKey = LocalDate.now() to LocalDate.now()

            val testActivePeriodsAndWeekdaysMap = mapOf(testMapKey to arrayOf(""))

            mainViewModel.onEvent(UpdateActivePeriodsAndWeekdaysMap(testActivePeriodsAndWeekdaysMap))

            mainViewModel.onEvent(AddAlarmToDatabase)

            backgroundScope.launch(UnconfinedTestDispatcher(testScheduler)) {
                mainViewModel.errorFlow.collectLatest {
                    assertThat(it).isInstanceOf(AlarmWeeklyRepetitionSettingsBlankException::class.java)
                }
            }
        }
    }

    /**
     * Test case that checks if the [DeleteAlarmFromDatabase] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event DeleteAlarmFromDatabase, alarm deleted`() {
        runTest {
            repository.insertAlarm(
                "Test",
                LocalDateTime.now(),
                mapOf(LocalDate.now() to LocalDate.now() to arrayOf("M", "W"))
            )

            mainViewModel.onEvent(FetchAlarmsFromDatabase)

            mainViewModel.onEvent(DeleteAlarmFromDatabase(0))

            assertThat(repository.alarms.count()).isEqualTo(0)
        }
    }

    /**
     * Test case that checks if the [ScheduleAlarm] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event ScheduleAlarm, alarm scheduled`() {

        runTest {
            repository.insertAlarm(
                "Test",
                LocalDateTime.now(),
                mapOf(LocalDate.now() to LocalDate.now().plusDays(7) to arrayOf("M", "W"))
            )

            mainViewModel.onEvent(FetchAlarmsFromDatabase)

            mainViewModel.onEvent(Event.ScheduleAlarm(0))

            assertThat(scheduler.scheduleCounter).isEqualTo(2)
        }
    }

    /**
     * Test case that checks if the [CancelAlarm] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event CancelAlarm, alarm cancelled`() {
        runTest {
            repository.insertAlarm(
                "Test",
                LocalDateTime.now(),
                mapOf(LocalDate.now() to LocalDate.now().plusDays(7) to arrayOf("M", "W"))
            )

            mainViewModel.onEvent(FetchAlarmsFromDatabase)

            mainViewModel.onEvent(Event.ScheduleAlarm(0))

            mainViewModel.onEvent(Event.CancelAlarm(0))

            assertThat(scheduler.scheduleCounter).isEqualTo(0)
        }
    }

    /**
     * Test case that checks if the [StartRescheduleWork] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event StartRescheduleWork, work started`() {
        mainViewModel.onEvent(StartRescheduleWork)

        assertThat(workManager.isPeriodicWorkStarted).isTrue()
    }

    /**
     * Test case that checks if the [StopRescheduleWork] event is handled properly
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `triggered event StopRescheduleWork, work stopped`() {
        mainViewModel.onEvent(StartRescheduleWork)

        mainViewModel.onEvent(StopRescheduleWork)

        assertThat(workManager.isPeriodicWorkStarted).isFalse()
    }
}