package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api

import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.WorkManagerApi

/**
 * The fake [WorkManagerApi] implementation for testing, which mimics the real one's behaviour
 * @author MaskedRedstonerProZ
 */
class FakeWorkManagerApi : WorkManagerApi {

    var isOneTimeWorkStarted: Boolean = false
        private set

    var isPeriodicWorkStarted: Boolean = false
        private set

    override fun startWork(
        workRequest: OneTimeWorkRequest,
        workName: String,
        workPolicy: ExistingWorkPolicy
    ) {
        if (isOneTimeWorkStarted) return

        isOneTimeWorkStarted = true
    }

    override fun startWork(
        workRequest: PeriodicWorkRequest,
        workName: String,
        workPolicy: ExistingPeriodicWorkPolicy
    ) {
        if (isPeriodicWorkStarted) return

        isPeriodicWorkStarted = true
    }

    override fun cancelWork(workName: String) {

        if (isOneTimeWorkStarted) {
            isOneTimeWorkStarted = false
            return
        }

        isPeriodicWorkStarted = false
    }
}