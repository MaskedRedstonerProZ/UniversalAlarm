package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.repository.FakeAlarmRepositoryImpl
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions.AlarmActivePeriodStartAfterEndException
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions.AlarmLabelLengthOverflowException
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions.AlarmWeeklyRepetitionSettingsBlankException
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.repository.AlarmRepository
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.Constants
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertThrows
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * The test class for the [AddAlarm] use case
 * @author MaskedRedstonerProZ
 */
class AddAlarmTest {

    /**
     * The use case instance for adding an [Alarm]
     * @author MaskedRedstonerProZ
     */
    private lateinit var addAlarm: AddAlarm

    /**
     * The test implementation of the [AlarmRepository]
     * @author MaskedRedstonerProZ
     */
    private lateinit var fakeRepository: FakeAlarmRepositoryImpl

    /**
     * The setup function that runs before every test case
     * @author MaskedRedstonerProZ
     */
    @Before
    fun setUp() {
        fakeRepository = FakeAlarmRepositoryImpl()
        addAlarm = AddAlarm(fakeRepository)
    }

    /**
     * Test case to test if adding a valid [Alarm] to the database results in success
     * @author MaskedRedstonerProZ
     */
     @Test
     fun `add valid alarm to database, success`() {

         val alarmToAdd = Alarm(
             id = 0,
             label = "Test Alarm",
             dueTime = LocalDateTime.now(),
             activePeriodsAndWeekdays = mapOf(LocalDate.now() to LocalDate.now() to arrayOf("M"))
         )

         runTest {
             addAlarm(alarmToAdd)

             assertThat(fakeRepository.alarms[0]).isEqualTo(alarmToAdd)
         }
     }

    /**
     * Test case to test if adding an [Alarm] with a label longer than the max length results in throwing the [AlarmLabelLengthOverflowException]
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `add alarm with a label past the max length, throws AlarmLabelLengthOverflowException`() {

        var label = ""

        (0..Constants.MAX_ALARM_LABEL_LENGTH).forEach {
            label += it.toString()[0]
        }

        val alarmToAdd = Alarm(
            id = 0,
            label = label,
            dueTime = LocalDateTime.now(),
            activePeriodsAndWeekdays = mapOf(LocalDate.now() to LocalDate.now() to arrayOf("M"))
        )

        assertThrows(AlarmLabelLengthOverflowException::class.java) {
            runTest {
                addAlarm(alarmToAdd)
            }
        }
    }

    /**
     * Test case to test if adding an [Alarm] with an active period
     * with a starting date 5 days past the ending date results in throwing the [AlarmActivePeriodStartAfterEndException]
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `add alarm with a startDate set to a one after the endDate, throws AlarmActivePeriodStartAfterEndException`() {

        val alarmToAdd = Alarm(
            id = 0,
            label = "",
            dueTime = LocalDateTime.now(),
            activePeriodsAndWeekdays = mapOf(LocalDate.now().plusDays(5) to LocalDate.now() to arrayOf("M"))
        )

        assertThrows(AlarmActivePeriodStartAfterEndException::class.java) {
            runTest {
                addAlarm(alarmToAdd)
            }
        }
    }

    /**
     * Test case to test if adding an [Alarm] with a weekly repetition settings array that contains an empty string
     * results in throwing the [AlarmWeeklyRepetitionSettingsBlankException]
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `add alarm with a weekly repetition settings array with an empty string, throws AlarmWeeklyRepetitionSettingsBlankException`() {

        val alarmToAdd = Alarm(
            id = 0,
            label = "",
            dueTime = LocalDateTime.now(),
            activePeriodsAndWeekdays = mapOf(LocalDate.now() to LocalDate.now() to arrayOf(""))
        )

        assertThrows(AlarmWeeklyRepetitionSettingsBlankException::class.java) {
            runTest {
                addAlarm(alarmToAdd)
            }
        }
    }

    /**
     * Test case to test if adding an [Alarm] with an empty activePeriodAndWeeklyRepetitionSettingsMap
     * results in throwing the [AlarmWeeklyRepetitionSettingsBlankException]
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `add alarm with an empty activePeriodAndWeeklyRepetitionSettingsMap, throws AlarmWeeklyRepetitionSettingsBlankException`() {
        val alarmToAdd = Alarm(
            id = 0,
            label = "",
            dueTime = LocalDateTime.now(),
            activePeriodsAndWeekdays = emptyMap()
        )

        assertThrows(AlarmWeeklyRepetitionSettingsBlankException::class.java) {
            runTest {
                addAlarm(alarmToAdd)
            }
        }
    }

    /**
     * Test case to test if adding an [Alarm] with an empty activePeriodAndWeeklyRepetitionSettingsMap
     * results in throwing the [AlarmWeeklyRepetitionSettingsBlankException]
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `add alarm with an empty weekly repetition settings array, throws AlarmWeeklyRepetitionSettingsBlankException`() {
        val alarmToAdd = Alarm(
            id = 0,
            label = "",
            dueTime = LocalDateTime.now(),
            activePeriodsAndWeekdays = mapOf(LocalDate.now() to LocalDate.now() to emptyArray())
        )

        assertThrows(AlarmWeeklyRepetitionSettingsBlankException::class.java) {
            runTest {
                addAlarm(alarmToAdd)
            }
        }
    }

}