package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.repository.AlarmRepository
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.exceptions.AlarmIdNonexistentException
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.repository.FakeAlarmRepositoryImpl
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertThrows
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * The test class for the [GetAlarm] use case
 * @author MaskedRedstonerProZ
 */
class GetAlarmTest {

    private lateinit var getAlarm: GetAlarm

    /**
     * The test implementation of the [AlarmRepository]
     * @author MaskedRedstonerProZ
     */
    private lateinit var fakeRepository: FakeAlarmRepositoryImpl

    /**
     * The setup function that runs before every test case
     * @author MaskedRedstonerProZ
     */
    @Before
    fun setUp() {
        fakeRepository = FakeAlarmRepositoryImpl()
        getAlarm = GetAlarm(fakeRepository)

        runTest {
            fakeRepository.insertAlarm("Test Alarm", LocalDateTime.now(), mapOf(Pair(Pair(LocalDate.now(), LocalDate.now()), arrayOf("M"))))
        }
    }

    /**
     * Test case to test if getting an [Alarm] with an existing id results in success
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `get alarm from database at id 0, success`() {
        runTest {
            val alarm = getAlarm(0)

            assertThat(alarm).isInstanceOf(Alarm::class.java)
        }
    }

    /**
     * Test case to test if getting an [Alarm] with an id that doesn't exist results in throwing the [AlarmIdNonexistentException]
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `get alarm from database at id 1, throws AlarmIdNonexistentException`() {
        assertThrows(AlarmIdNonexistentException::class.java) {
            runTest {
                getAlarm(1)
            }
        }
    }
}