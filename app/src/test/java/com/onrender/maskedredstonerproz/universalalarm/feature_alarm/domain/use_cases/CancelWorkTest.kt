package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api.FakeWorkManagerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.WorkManagerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.framework.workers.AlarmActivePeriodLoopWorker
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

/**
 * The test class for the [CancelWork] use case
 * @author MaskedRedstonerProZ
 */
class CancelWorkTest {

    /**
     * The use case instance for cancelling a worker
     * @author MaskedRedstonerProZ
     */
    private lateinit var cancelWork: CancelWork

    /**
     * The test implementation of the [WorkManagerApi]
     * @author MaskedRedstonerProZ
     */
    private lateinit var workManager: FakeWorkManagerApi

    /**
     * The setup function that runs before every test case
     * @author MaskedRedstonerProZ
     */
    @Before
    fun setUp() {
        workManager = FakeWorkManagerApi()
        cancelWork = CancelWork(workManager)
    }

    /**
     * Test case that tests if attempting to cancel a periodic work results in it being actually cancelled
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `cancel periodic work with the AlarmActivePeriodLoopWorker, cancelled`() {

        val workRequest =
            PeriodicWorkRequestBuilder<AlarmActivePeriodLoopWorker>(1, TimeUnit.DAYS)
                .build()

        workManager.startWork(workRequest, "Test work", ExistingPeriodicWorkPolicy.KEEP)

        cancelWork("Test work")

        assertThat(workManager.isPeriodicWorkStarted).isFalse()
    }
}