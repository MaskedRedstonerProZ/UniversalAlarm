package com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.use_cases

import com.google.common.truth.Truth.assertThat
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.data.api.FakeAlarmSchedulerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.api.AlarmSchedulerApi
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.model.Alarm
import com.onrender.maskedredstonerproz.universalalarm.feature_alarm.domain.util.runBasedOnDueDateOf
import org.junit.Before
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * The test class for the [CancelAlarm] use case
 * @author MaskedRedstonerProZ
 */
class CancelAlarmTest {

    /**
     * The use case instance for cancelling an [Alarm]
     * @author MaskedRedstonerProZ
     */
    private lateinit var cancelAlarm: CancelAlarm

    /**
     * The test implementation instance of the [AlarmSchedulerApi]
     * @author MaskedRedstonerProZ
     */
    private lateinit var scheduler: FakeAlarmSchedulerApi

    /**
     * The setup function that runs before every test case
     * @author MaskedRedstonerProZ
     */
    @Before
    fun setUp() {
        scheduler = FakeAlarmSchedulerApi()
        cancelAlarm = CancelAlarm(scheduler)
    }

    /**
     * Test case to test if cancelling an alarm results in it being cancelled for every required day within the active period
     * @author MaskedRedstonerProZ
     */
    @Test
    fun `cancel alarm, cancelled for every chosen weekday in active period`() {

        val weekdayRepetitionSettings = arrayOf(
            "M",
            "T",
            "W",
            "TH",
            "F",
            "S",
            "SU"
        )

        val alarmToSchedule = Alarm(
            id = 1,
            label = "Test alarm",
            dueTime = LocalDateTime.now(),
            activePeriodsAndWeekdays = mapOf(
                LocalDate.now() to LocalDate.now().plusDays(7) to weekdayRepetitionSettings
            )
        )

        runBasedOnDueDateOf(alarmToSchedule) {
            scheduler.schedule(alarmToSchedule.copy(
                dueTime = LocalDateTime.of(it, alarmToSchedule.dueTime.toLocalTime())
            ))
        }

        cancelAlarm(alarmToSchedule)

        assertThat(scheduler.scheduleCounter).isEqualTo(0)
    }

}
